<?php require_once('../includes/config.php'); ?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<title>Self Coaching 360º Desarrollo personal y liderazgo</title>
        <meta charset="utf-8">
		<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
		<!--[if lte IE 8]>
		<script src="<?php echo ETG_BASE_URL; ?>/http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<!--[if lt IE 8]>
			<script src="<?php echo ETG_BASE_URL; ?>/http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
		<![endif]-->
		<link rel="shortcut icon" href="<?php echo ETG_BASE_URL; ?>/favicon.ico" type="image/x-icon" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/bxslider.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/font-awesome.min.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/selectric.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/style.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/adaptive.css" media="screen" />
		
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/jquery.selectric.min.js"></script>
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/jquery.bxslider.min.js"></script>
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/script.js"></script>
	</head>
<body class="single-post">
    
    <?php 
        $current = 'master-360';
        require_once('../includes/cabecera.php');
    ?>

	<section class="container">
		<div class="pageHeader" style="background-image: url(<?php echo ETG_BASE_URL; ?>/images/master-360/interiores/06.jpg);">
			<h1>QUé NOS HACE GENUINOS</h1>
		</div>
		<div class="wrapper">
			<div class="singlePostWrap">    
                <h2>La autenticidad de lo que proponemos.</h2>
                <p>A la hora de concebir cada contenido hemos puesto el foco en garantizar máxima calidad y añadiendo kilos y kilos de autenticidad a cada minuto. Eso es lo que marca la diferencia.</p>
                
                <h2>Pasión.</h2>
                <p>A cada persona de nuestro equipo nos encanta lo que hacemos y creemos en lo que decimos. Eso se nota en los detalles y se contagia. Lo llevamos en el ADN.</p>
                
                <h2>Práctica, práctica, práctica.</h2>
                <p>Creemos que el desarrollo personal y el liderazgo hay que vivirlo para saber de ello, poniendo en práctica la teoría desde el minuto uno. Esta formación experiencial centra el foco en que todas las personas vivan el proceso en primera persona y se conviertan en protagonistas.</p>

                <h2>Credibilidad.</h2>
                <p>La pasión y la autenticidad están muy bien, aunque mucho aire sin poner los pies en el suelo puede ser un desastre. Por eso cada profesional que forma parte del equipo de este programa cuenta con una trayectoria y especialidad demostrable en el área que imparte. Zapatero a tus zapatos.</p>
                
                <h2>Sin barreras.</h2>
                <p>Juntos y revueltos, en la diversidad está la riqueza. Este programa propone cuestionarse moldes y limitaciones. Quienes acuden son personas con inquietudes y con la mente abierta a nuevas ideas. No me define lo que hago ni lo que tengo sino el tipo de persona que soy.</p>
                
                <h2>Escuchar lo que de verdad importa.</h2>
                <p> Nos referimos a desarrollar la empatía con los problemas del día a día de los participantes. No es un programa donde desde un púlpito damos un regla mágica y te arreglas. Siempre intentamos ponernos en los zapatos de quien acude y ayudarle a jugar a su mejor juego.</p>
            </div>
        </div>
		
    <?php require_once('../includes/formulario.php'); ?>
		
	</section>

    <?php require_once('../includes/pie.php'); ?>   
    
</body>
</html>