<?php require_once('../includes/config.php'); ?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<title>Self Coaching 360º Desarrollo personal y liderazgo</title>
        <meta charset="utf-8">
		<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
		<!--[if lte IE 8]>
		<script src="<?php echo ETG_BASE_URL; ?>/http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<!--[if lt IE 8]>
			<script src="<?php echo ETG_BASE_URL; ?>/http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
		<![endif]-->
		<link rel="shortcut icon" href="<?php echo ETG_BASE_URL; ?>/favicon.ico" type="image/x-icon" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/bxslider.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/font-awesome.min.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/selectric.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/style.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/adaptive.css" media="screen" />
		
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/jquery.selectric.min.js"></script>
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/jquery.bxslider.min.js"></script>
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/script.js"></script>
	</head>
<body class="single-post">
    
    <?php 
        $current = 'master-360';
        require_once('../includes/cabecera.php');
    ?>

	<section class="container">
		<div class="pageHeader" style="background-image: url(<?php echo ETG_BASE_URL; ?>/images/master-360/interiores/02.jpg);">
			<h1>UN MÁSTER DE MÁSTERES</h1>
		</div>
		<div class="wrapper">
			<div class="singlePostWrap">    
                <p><strong>Un Máster compuesto de 5 poderosos programas independientes que completan un viaje de 360º en la vida del ser humano.</strong></p>
                <ul class="roboto">
                    <li>Autoconocimiento y realización personal</li>
                    <li>Educación emocional y la mente         </li>
                    <li>Comunicación y relaciones              </li>
                    <li>Salud y bienestar                      </li>
                    <li>Abundancia y resultados                </li>
                </ul>
                
                <p><strong>Un Máster que ofrece soluciones sencillas a problemas complejos en 10 temas universales:</strong></p>
                
                <ul>
                    <li>Felicidad y sentido</li>
                    <li>Plenitud personal y + poder interior</li>
                    <li>Superación de límites</li>
                    <li>Amor y pareja</li>
                    <li>Educación de nuestros hijos</li>
                    <li>Relaciones</li>
                    <li>Salud y bienestar</li>
                    <li>Trascendencia y legado</li>
                    <li>Realización profesional</li>
                    <li>Dinero y finanzas</li>
                </ul>
            </div>
        </div>
    <?php require_once('../includes/formulario.php'); ?>
		
	</section>

    <?php require_once('../includes/pie.php'); ?>   
</body>
</html>