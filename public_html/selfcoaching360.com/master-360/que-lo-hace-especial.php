<?php require_once('../includes/config.php'); ?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<title>Self Coaching 360º Desarrollo personal y liderazgo</title>
        <meta charset="utf-8">
		<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
		<!--[if lte IE 8]>
		<script src="<?php echo ETG_BASE_URL; ?>/http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<!--[if lt IE 8]>
			<script src="<?php echo ETG_BASE_URL; ?>/http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
		<![endif]-->
		<link rel="shortcut icon" href="<?php echo ETG_BASE_URL; ?>/favicon.ico" type="image/x-icon" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/bxslider.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/font-awesome.min.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/selectric.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/style.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/adaptive.css" media="screen" />
		
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/jquery.selectric.min.js"></script>
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/jquery.bxslider.min.js"></script>
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/script.js"></script>
	</head>
<body class="single-post">
    
    <?php 
        $current = 'master-360';
        require_once('../includes/cabecera.php');
    ?>

	<section class="container">
		<div class="pageHeader" style="background-image: url(<?php echo ETG_BASE_URL; ?>/images/master-360/interiores/04.jpg);">
			<h1>CARACTERÍSTICAS</h1>
		</div>
		<div class="wrapper">
			<div class="singlePostWrap">    
                <p><strong>LA SUMA DE ESTAS CARACTERÍSTICAS LO HACEN ESPECIAL...</strong></p>
                <h2>100% práctico y experiencial.</h2>
                <p>Para experimentar en primera persona el desarrollo personal.</p>
                
                <h2>Acompañamiento continuo.</h2>
                <p>Contarás con un equipo de profesionales de la Escuela Selfcoaching 360 que te asesorará para diseñar tu plan de formación a medida.</p>
                
                <h2>Ser tu propio coach. </h2>
                <p>El objetivo de esta formación vivencial es que durante el Máster descubras tu potencial y las herramientas necesarias para ser tu propio coach.</p>
                
                <h2>Coaching grupal.</h2>
                <p>Y una vez te inscribas, paralelamente al Máster podrás participar de sesiones de coaching grupal según el plan de formación personal que hayas escogido para reforzar tus progresos. Verás que la fuerza del equipo es una influencia positiva enorme para el logro de tu objetivo.</p>
                
                <h2>Abierto a todas las personas.</h2>
                <p>No hay requisitos de edad ni necesitas acreditar una formación previa de ningún tipo para acceder al Máster.</p>
                
                <h2>Historias extraordinarias para personas únicas.</h2>
                <p>Cada uno de los cinco programas del Máster contará con una o más historias que ponen en contexto el contenido que se imparte. Conocerás a personas sorprendentes, especiales, auténticas y que son ejemplo de aquello que te presentamos.</p>
                
                <h2>Subvencionable vía Fundación Tripartita.</h2>
                <p>Esta formación puedes incluirla en el programa de la Fundación Tripartita de tu empresa y beneficiarte de la subvención correspondiente.</p>
                 
                <h2>Pensado para que puedas dedicarte profesionalmente a este sector.</h2>
                <p>Si quisieras encontrar una salida profesional en el mundo del desarrollo personal, recursos humanos, liderazgo o dirección de empresas, el Máster está pensado para que se convierta en la lanzadera para encontrar tu especialización y llegar a ser el coach de otras personas. Para ello podrás acceder al programa profesional donde conseguirás la Certificación Profesional ES360 (Escuela Selfcoaching 360).</p>
                
                <h2>Entrevista previa imprescindible. </h2>
                <p>Es la única condición que la Escuela de Selfcoaching y Liderazgo establece para participar. Para ello, cada persona candidata enviará una carta motivacional a la hora de preinscribirse y, a posteriori la escuela atenderá cada candidatura con una entrevista personal. En esa entrevista personal se aclararán todas las dudas. Tras la entrevista, la escuela se reserva el derecho a dar el visto bueno a la inscripción final.</p>
                
                <h2>Formación presencial con apoyo de webinarios on line.</h2>
                <p>El 80% del Máster es presencial en la ciudad de Donostia-San Sebastián. Cada uno de los cinco programas lo forman de 4 a 6 módulos presenciales complementados con dos conferencias interactivas on line en directo con dos personas de nuestro equipo.</p>
                
                <h2>Acreditado y Certificado ES360. </h2>
                <p>Tras la realización de cada uno de los cinco programas recibirás la acreditación correspondiente de cada programa expedida por la Escuela de Selfcoaching y Liderazgo. Y completando los 5 programas recibirás el título con la Certificación Oficial de la Escuela que garantiza el cumplimiento del Máster. Para ello presentarás, según las pautas que te indiquemos, el proyecto Hábitos al finalizar tus tres primeros programas y el proyecto Experiencia tras acabar el quinto.</p>
 
            </div>
        </div>
		
    <?php require_once('../includes/formulario.php'); ?>
		
	</section>

    <?php require_once('../includes/pie.php'); ?>   
    
</body>
</html>