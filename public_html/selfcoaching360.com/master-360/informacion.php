<?php require_once('../includes/config.php'); ?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<title>Self Coaching 360º Desarrollo personal y liderazgo</title>
        <meta charset="utf-8">
		<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
		<!--[if lte IE 8]>
		<script src="<?php echo ETG_BASE_URL; ?>/http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<!--[if lt IE 8]>
			<script src="<?php echo ETG_BASE_URL; ?>/http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
		<![endif]-->
		<link rel="shortcut icon" href="<?php echo ETG_BASE_URL; ?>/favicon.ico" type="image/x-icon" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/bxslider.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/font-awesome.min.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/selectric.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/style.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/adaptive.css" media="screen" />
		
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/jquery.selectric.min.js"></script>
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/jquery.bxslider.min.js"></script>
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/script.js"></script>
	</head>
<body class="single-post">
    
    <?php 
        $current = 'master-360';
        require_once('../includes/cabecera.php');
    ?>

	<section class="container">
		<div class="pageHeader" style="background-image: url(<?php echo ETG_BASE_URL; ?>/images/master-360/interiores/10.jpg);">
			<h1>DóNDE, CóMO, CUáNTO, POR QUé</h1>
		</div>
		<div class="wrapper">
			<div class="singlePostWrap">    
                <h2>¿DÓNDE SE IMPARTE? UNA EXPERIENCIA ÚNICA NECESITA ESPACIOS AUTÉNTICOS.</h2>
                
                <p>Cada detalle es importante para crear el clima adecuado en cada momento. Los 5 programas se desarrollan en Donostia-San Sebastián. En dos enclaves inspiradores, cómodos, atractivos y bien comunicados.</p>

                <p><strong>Colegio Mayor Olarain</strong>.<br />
                    En un entorno universitario. Es un chute de frescura y originalidad.
                </p>
                <p><img src="../images/olarain.jpg" alt="olarain" class="img-responsive center-block" style="width: 100%; height: auto"></p>

                <p>
                    <strong>Espacio IMPACT HUB</strong><br />
                    Dentro del emblemático Edificio Tabakalera. Un lugar dinámico, abierto y creativo, que no deja a nadie indiferente.
                </p>
                    
                <p><img src="../images/tabakalera.jpg" alt="tabakalera" class="img-responsive center-block"style="width: 100%; height: auto"></p>

                
                <h3>Precios</h3>
                <table>
                    <tr>
                        <td>[Lanzadera] Autoconocimiento y liderazgo</td>
                        <td style="text-align: right">600€</td>
                    </tr>
                    <tr>
                        <td>Educación emocional y entrenamiento mental</td>
                        <td style="text-align: right">880€</td>
                    </tr>
                    <tr>
                        <td>Comunicación y relaciones</td>
                        <td style="text-align: right">880€</td>
                    </tr>
                    <tr>
                        <td>Salud y bienestar</td>
                        <td style="text-align: right">880€</td>
                    </tr>
                    <tr>
                        <td>Abundancia y resultados</td>
                        <td style="text-align: right">830€</td>
                    </tr>
                    <tr>
                        <td>Máster completo</td>
                        <td style="text-align: right">4070€</td>
                    </tr>
                </table>
                
                
                
                <h3>Becas</h3>
                <p>Benefíciate de descuentos importantes según nuestro plan de becas. Para ello solicita información a través de nuestra web: www.selfcoaching360.com</p>

                
                <h3>Formas de pago</h3>
                <p>Te facilitamos formas de pago adaptadas a tu situación. Te atenderemos personalmente.</p>
                
                
                <h3>Financiación</h3>
                <p>Y si necesitas financiación, la Escuela Selfcoaching 360 te la puede facilitar gracias a sus acuerdos con algunas entidades bancarias. Solicítanos información. </p>
                
                
                <p>
                    * Precios finales. IVA incluido. <br />
                    * Estos precios no incluyen los 90€ del precio de la matrícula que sólo abonarás la primera vez que te inscribas.<br />
                    * Formación subvencionable vía Fundación Tripartita.                    
                </p>
            </div>
        </div>
		
    <?php require_once('../includes/formulario.php'); ?>
		
	</section>

    <?php require_once('../includes/pie.php'); ?>   
    
</body>
</html>

