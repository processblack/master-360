<?php require_once('../includes/config.php'); ?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<title>Self Coaching 360º - Desarrollo personal y liderazgo</title>
		<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
		<!--[if lte IE 8]>
		<script src="<?php echo ETG_BASE_URL; ?>/http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<!--[if lt IE 8]>
			<script src="<?php echo ETG_BASE_URL; ?>/http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
		<![endif]-->
		<link rel="shortcut icon" href="<?php echo ETG_BASE_URL; ?>/favicon.ico" type="image/x-icon" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/bxslider.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/font-awesome.min.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/selectric.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/style.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/adaptive.css" media="screen" />
		
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/jquery.selectric.min.js"></script>
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/jquery.bxslider.min.js"></script>
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/script.js"></script>
	</head>
<body class="blog">
    
    <?php 
        $current = 'master-360';
        require_once('../includes/cabecera.php');
    ?>

	<section class="container">
		<div class="blogPostWrap">
			<div class="postItem">
				<a href="origen-selfcoaching.php" class="postItemImg">
					<img src="<?php echo ETG_BASE_URL; ?>/images/master-360/01.jpg" alt="">
				</a>
				<h4><a href="origen-selfcoaching.php">El origen</a></h4>
				<!--<p>Duis aute irure dolor in reprehenderit in velit esse cillum dolore eu fugiat nulla pariatur.</p>-->
			</div>
			<div class="postItem">
				<a href="master-de-masteres.php" class="postItemImg">
					<img src="<?php echo ETG_BASE_URL; ?>/images/master-360/02.jpg" alt="">
				</a>
				<h4><a href="master-de-masteres.php">Un máster de másteres</a></h4>
				<!--<p>Duis aute irure dolor in reprehenderit in velit esse cillum dolore eu fugiat nulla pariatur.</p>-->
			</div>
			<div class="postItem">
				<a href="te-interesa-si.php" class="postItemImg">
					<img src="<?php echo ETG_BASE_URL; ?>/images/master-360/03.jpg" alt="">
				</a>
				<h4><a href="te-interesa-si.php">Te interesa si...</a></h4>
				<!--<p>Duis aute irure dolor in reprehenderit in velit esse cillum dolore eu fugiat nulla pariatur.</p>-->
			</div>
			<div class="postItem">
				<a href="que-lo-hace-especial.php" class="postItemImg">
					<img src="<?php echo ETG_BASE_URL; ?>/images/master-360/04.jpg" alt="">
				</a>
				<h4><a href="que-lo-hace-especial.php">CARACTERÍSTICAS</a></h4>
				<!--<p>Duis aute irure dolor in reprehenderit in velit esse cillum dolore eu fugiat nulla pariatur.</p>-->
			</div>
			<div class="postItem">
				<a href="experiencia-a-la-carta.php" class="postItemImg">
					<img src="<?php echo ETG_BASE_URL; ?>/images/master-360/05.jpg" alt="">
				</a>
				<h4><a href="experiencia-a-la-carta.php">Experiencia a la carta</a></h4>
				<!--<p>Duis aute irure dolor in reprehenderit in velit esse cillum dolore eu fugiat nulla pariatur.</p>-->
			</div>
			<div class="postItem">
				<a href="genuinos.php" class="postItemImg">
					<img src="<?php echo ETG_BASE_URL; ?>/images/master-360/06.jpg" alt="">
				</a>
				<h4><a href="genuinos.php">Qué nos hace genuinos</a></h4>
				<!--<p>Duis aute irure dolor in reprehenderit in velit esse cillum dolore eu fugiat nulla pariatur.</p>-->
			</div>
			<div class="postItem">
				<a href="metodologia-experiencial.php" class="postItemImg">
					<img src="<?php echo ETG_BASE_URL; ?>/images/master-360/07.jpg" alt="">
				</a>
				<h4><a href="metodologia-experiencial.php">Metodología experiencial</a></h4>
				<!--<p>Duis aute irure dolor in reprehenderit in velit esse cillum dolore eu fugiat nulla pariatur.</p>-->
			</div>
			<div class="postItem">
				<a href="el-master-en-numeros.php" class="postItemImg">
					<img src="<?php echo ETG_BASE_URL; ?>/images/master-360/08.jpg" alt="">
				</a>
				<h4><a href="el-master-en-numeros.php">El máster en números</a></h4>
				<!--<p>Duis aute irure dolor in reprehenderit in velit esse cillum dolore eu fugiat nulla pariatur.</p>-->
			</div>
			<div class="postItem">
				<a href="poderosos-programas.php" class="postItemImg">
					<img src="<?php echo ETG_BASE_URL; ?>/images/master-360/09.jpg" alt="">
				</a>
				<h4><a href="poderosos-programas.php">5 Poderosos programas</a></h4>
				<!--<p>Duis aute irure dolor in reprehenderit in velit esse cillum dolore eu fugiat nulla pariatur.</p>-->
			</div>
			<div class="postItem">
				<a href="informacion.php" class="postItemImg">
					<img src="<?php echo ETG_BASE_URL; ?>/images/master-360/10.jpg" alt="">
				</a>
				<h4><a href="informacion.php">Dónde, cómo, cuánto, por qué</a></h4>
				<!--<p>Duis aute irure dolor in reprehenderit in velit esse cillum dolore eu fugiat nulla pariatur.</p>-->
			</div>
		</div>
    <?php require_once('../includes/descarga-dossier.php'); ?>
    <?php require_once('../includes/formulario.php'); ?>
		
	</section>

    <?php require_once('../includes/pie.php'); ?>   
    
</body>
</html>