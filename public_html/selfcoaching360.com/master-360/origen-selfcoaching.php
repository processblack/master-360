<?php require_once('../includes/config.php'); ?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<title>Self Coaching 360º Desarrollo personal y liderazgo</title>
        <meta charset="utf-8">
		<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
		<!--[if lte IE 8]>
		<script src="<?php echo ETG_BASE_URL; ?>/http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<!--[if lt IE 8]>
			<script src="<?php echo ETG_BASE_URL; ?>/http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
		<![endif]-->
		<link rel="shortcut icon" href="<?php echo ETG_BASE_URL; ?>/favicon.ico" type="image/x-icon" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/bxslider.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/font-awesome.min.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/selectric.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/style.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/adaptive.css" media="screen" />
		
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/jquery.selectric.min.js"></script>
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/jquery.bxslider.min.js"></script>
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/script.js"></script>
	</head>
<body class="single-post">
    
    <?php 
        $current = 'master-360';
        require_once('../includes/cabecera.php');
    ?>

	<section class="container">
		<div class="pageHeader" style="background-image: url(<?php echo ETG_BASE_URL; ?>/images/master-360/interiores/01.jpg); background-position: center bottom">
			<h1>EL ORIGEN</h1>
		</div>
		<div class="wrapper">
			<div class="singlePostWrap">    
        		<h2>1ª reflexión:</h2>
                <p><strong class="azul">Este Máster Selfcoaching 360º surge con el propósito de aportar claridad en ese bosque de tanta oferta formativa.</strong></p>
                <p>Hay muchos cursos de crecimiento personal y liderazgo francamente buenos impartidos por excelentes profesionales. El problema es que generalmente se encuentran desperdigados y es difícil encontrarlos cuando los necesitas. No hay un espacio donde te informen de qué formación te puede venir bien en el momento en el que te encuentras.</p>
                
                
                
                <h2>2ª reflexión:</h2>
                <p><strong class="azul">El Máster Selfcoaching 360º está cuidadosamente diseñado, combinando técnicas y métodos poderosos del crecimiento personal en beneficio de cada participante.</strong></p>
                <p>Hay grandes herramientas, metodologías y técnicas como el selfcoaching, coaching holístico, PNL, Educación emocional, coaching sistémico, mindfulness, eneagrama, constelaciones, análisis transaccional, pensamiento positivo, etc. que, por sí solas, ayudan en áreas concretas del desarrollo personal; y que si las complementamos podemos conseguir resultados magníficos y en menor tiempo. Sacando lo mejor de cada técnica.</p>
                
                
                <h2>3ª reflexión:</h2>
                <p><strong class="azul">Este Máster está concebido como un manual de instrucciones del ser humano, donde poder encontrar soluciones sencillas a problemas complejos.</strong></p>
                <p>El nuevo paradigma social y el cambiante entorno profesional exige personas autosuficientes, con capacidad de adaptación a los nuevos retos. Pero las Escuelas y las Universidades, en general, no incluyen una formación específica que ayude a comprender el comportamiento humano y desarrolle las capacidades personales.</p>
            </div>
        </div>
		
    <?php require_once('../includes/formulario.php'); ?>
		
	</section>

    <?php require_once('../includes/pie.php'); ?>   
    
</body>
</html>