<?php require_once('../includes/config.php'); ?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<title>Self Coaching 360º Desarrollo personal y liderazgo</title>
        <meta charset="utf-8">
		<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
		<!--[if lte IE 8]>
		<script src="<?php echo ETG_BASE_URL; ?>/http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<!--[if lt IE 8]>
			<script src="<?php echo ETG_BASE_URL; ?>/http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
		<![endif]-->
		<link rel="shortcut icon" href="<?php echo ETG_BASE_URL; ?>/favicon.ico" type="image/x-icon" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/bxslider.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/font-awesome.min.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/selectric.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/style.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/adaptive.css" media="screen" />
		
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/jquery.selectric.min.js"></script>
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/jquery.bxslider.min.js"></script>
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/script.js"></script>
	</head>
<body class="single-post">
    
    <?php 
        $current = 'master-360';
        require_once('../includes/cabecera.php');
    ?>

	<section class="container">
		<div class="pageHeader" style="background-image: url(<?php echo ETG_BASE_URL; ?>/images/master-360/interiores/07.jpg);">
			<h1 style="text-transform: uppercase;">Metodología Experiencial</h1>
		</div>
		<div class="wrapper">
			<div class="singlePostWrap">    
                <p>La verdadera transformación sucede cuando te atreves a probar y a experimentar en primera persona los conocimientos adquiridos. Este Máster de Másteres está diseñado para todas aquellas personas que quieren crecer, evolucionar y generar influencia positiva en su entorno.</p>
                <p><img src="<?php echo ETG_BASE_URL; ?>/images/metodologia.png" alt="metodologia" class="img-responsive center-block">
            </div>
        </div>
		
    <?php require_once('../includes/formulario.php'); ?>
		
	</section>

    <?php require_once('../includes/pie.php'); ?>   
    
</body>
</html>