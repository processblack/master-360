<?php require_once('../includes/config.php'); ?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<title>Self Coaching 360º Desarrollo personal y liderazgo</title>
        <meta charset="utf-8">
		<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
		<!--[if lte IE 8]>
		<script src="<?php echo ETG_BASE_URL; ?>/http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<!--[if lt IE 8]>
			<script src="<?php echo ETG_BASE_URL; ?>/http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
		<![endif]-->
		<link rel="shortcut icon" href="<?php echo ETG_BASE_URL; ?>/favicon.ico" type="image/x-icon" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/bxslider.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/font-awesome.min.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/selectric.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/style.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/adaptive.css" media="screen" />
		
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/jquery.selectric.min.js"></script>
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/jquery.bxslider.min.js"></script>
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/script.js"></script>
	</head>
<body class="single-post">
    
    <?php 
        $current = 'master-360';
        require_once('../includes/cabecera.php');
    ?>

	<section class="container">
		<div class="pageHeader" style="background-image: url(<?php echo ETG_BASE_URL; ?>/images/master-360/interiores/05.jpg);">
			<h1>EXPERIENCIA A LA CARTA</h1>
		</div>
		<div class="wrapper">
			<div class="singlePostWrap">    
                <p><strong>4 FACTORES ESTRELLA QUE HACEN QUE ESTE MÁSTER SEA UNA EXPERIENCIA “A LA CARTA”</strong></p>
                <h2>A tu medida.</h2>
                <p>Puedes disfrutar de los 5 programas del máster o elegir sólo los programas que más se ajustan a tu realidad y necesidades actuales.</p>
                
                <h2>Concilia tu vida personal y profesional.</h2>
                <p>Puedes vivirlo en uno, dos y hasta en tres años.</p>
                                
                <h2>En este Máster el camino lo eliges tú. </h2>
                <p>En el crecimiento personal hay muchas maneras de llegar a tu destino, no hay un único trayecto. En cambio, la mayoría de los cursos de formación están diseñados con un itinerario fijo, con un programa marcado por el que te obligan a transitar de principio a fin. En este máster la metodología es diferente. Empezarás por el programa lanzadera (Autoconocimiento y Realización Personal) y el resto de la aventura la construyes a tu medida. Decidirás qué programa realizar, en qué orden y cuándo. Te asesoraremos en todo el camino.</p>
                
                <h2>Adaptado a tu situación económica.</h2>
                <p>Puedes vivir en primera persona una experiencia transformadora desde sólo 500€. Y puedes inscribirte a más de un programa beneficiándote de promociones especiales y facilidades de financiación a tu medida.</p>
                
            </div>
        </div>
		
    <?php require_once('../includes/formulario.php'); ?>
		
	</section>

    <?php require_once('../includes/pie.php'); ?>   
    
</body>
</html>