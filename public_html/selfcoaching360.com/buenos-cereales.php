<?php require_once('includes/config.php'); ?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<title>Self Coaching 360º Desarrollo personal y liderazgo</title>
		<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
		<!--[if lte IE 8]>
		<script src="<?php echo ETG_BASE_URL; ?>/http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<!--[if lt IE 8]>
			<script src="<?php echo ETG_BASE_URL; ?>/http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
		<![endif]-->
		<link rel="shortcut icon" href="<?php echo ETG_BASE_URL; ?>/favicon.ico" type="image/x-icon" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/bxslider.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/font-awesome.min.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/selectric.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/style.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/adaptive.css" media="screen" />
		
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/jquery.selectric.min.js"></script>
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/jquery.bxslider.min.js"></script>
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/script.js"></script>
	</head>
<body class="single-post">
    
    <?php 
        $current = 'equipo';
        require_once('includes/cabecera.php');
    ?>

	<section class="container">
		<div class="pageHeader" style="background-image: url(<?php echo ETG_BASE_URL; ?>/images/buenos-cereales-hero-2-black.jpg);">
			<h1 style="top: 180px;">
    			<span class="little">INVITACIÓN</span><br />
    			DESAYUNANDO BUENOS CEREALES
            </h1>
		</div>
		
		
    	
		<div class="wrapper">
			<div class="singlePostWrap">
    			<h4 style="font-family: 'Roboto Condensed', sans-serif;">LOS DESAYUNOS DE LA ESCUELA SELFCOACHING 360º DEL PAÍS VASCO</h4>
    			<p>Desayunando Buenos Cereales es una invitación de la <a href="http://selfcoaching360.com">Escuela de Self Coaching 360º</a> del País Vasco. Se trata de una iniciativa dirigida por Iker Fernández, consultor homologado por el Gobierno Vasco, <strong>destinada a empresas que quieren mejorar la productividad de las personas y los equipos</strong>.</p>
    			
            
    			
    			
						<h4 style="font-family: 'Roboto Condensed', sans-serif;">Sede GBC - Próximo evento </h4>
						<p>
                            <strong>Día:</strong> Martes 13 Febrero<br />
    					    <strong>Horario:</strong> 09.00h a 11.00h<br>
    					    <strong>Precio:</strong> Sólo con invitación<br>
    					    <strong>Lugar:</strong> Agirre Miramon Kalea, 2<br />Plaza de Toros de Illumbe.<br />Entrada por la Puerta 3.
    				    </p>
                        <p style="text-align: center;"><a href="https://www.eventbrite.es/e/entradas-desayunando-buenos-cereales-gestion-de-personas-equipos-40788593782" target="_blank" class="descarga">INSCRIBIRME</a></p>


                <p><strong>Colaboran</strong></p>
                <p>
                    <img src="<?php echo ETG_BASE_URL; ?>/images/cereales-logos.png" alt="Kirolgi - San Sebastián Gipuzkoa Basket" class="img-responsive" />
                </p>

    			
    
            </div>
        </div>
        
		<div class="pageHeader" style="background-image: url(<?php echo ETG_BASE_URL; ?>/images/buenos-cereales.jpg);">
    		<h1>OBJETIVOS</h1>
		</div>
        



		<div class="wrapper">
			<div class="singlePostWrap">    
                    
                <p>Se busca crear un espacio de reflexión para las empresas. Un momento afable y distendido destinado a parar y pensar. Esta iniciativa pretende:</p>
                
                <div class="numero">
                <h4>1.</h4>
                <p>Compartir el día a día en el ámbito de la gestión de personas y equipos.</p>
                </div>
                
                <div class="numero">
                <h4>2.</h4>
                <p>Lograr que cada asistente se lleve un herramienta práctica.</p>
                </div>
                
                <div class="numero">
                <h4>3.</h4>
                <p>Vivir una experiencia.</p>
                </div>
                
                <div class="numero">
                <h4>4.</h4>
                <p>Establecer contactos con otros profesionales.</p>

                </div>
                
                        <p style="text-align: center;"><a href="https://www.eventbrite.es/e/entradas-desayunando-buenos-cereales-gestion-de-personas-equipos-40788593782" target="_blank" class="descarga">INSCRIBIRME</a></p>
                
            </div>
        </div>
                
                
		<div class="pageHeader" style="background-image: url(<?php echo ETG_BASE_URL; ?>/images/cereales-hero-01-top.jpg);">
			<h1 class="">Dirigido a personas que gestionan personas</h1>
		</div>
                
		<div class="wrapper">
			<div class="singlePostWrap">    
                <h2>¿POR QUÉ<br />SELFCOACHING 360?</h2>
                
                <p>Porque nuestra escuela ha desarrollado un programa pionero en Euskadi de formación experiencial en Liderazgo y Equipos.</p>
                <p>Es un programa que suma a la fase teórica y práctica tradicional, un tercer pilar, “Vivir la experiencia” que nos hace anclar las dinámicas vividas de tal forma que mejora en un 80% el aprendizaje.</p>
                
                
                <h2>¿QUÉ<br />CONSEGUIMOS?</h2>
                <p>Cada asistente se llevará una reflexión y una herramienta práctica para pasar a la acción.</p>
                
                        <p style="text-align: center;"><a href="https://www.eventbrite.es/e/entradas-desayunando-buenos-cereales-gestion-de-personas-equipos-40788593782" target="_blank" class="descarga">INSCRIBIRME</a></p>
                
            </div>
        </div>
                
                
		<div class="pageHeader" style="background-image: url(<?php echo ETG_BASE_URL; ?>/images/buenos-cereales-hero-3.jpg);">
		</div>
                
		<div class="wrapper">
			<div class="singlePostWrap">    
                
                <h2>¿CÓMO<br />FUNCIONA?</h2>
                <p>Desayunando Buenos Cereales es una experiencia de dos horas de duración para 10 asistentes invitados personalmente.</p>
                <p>Mediante la conexión del mundo del deporte y la empresa, Iker Fernández comparte la metodología de las 4P para la gestión de personas y equipos: Parar, Pensar, Proponer y Probar.</p>
                
                
    			<h2>¿CUÁNDO Y DONDE?</h2>
    			<p>A lo largo del año se realizan 4 eventos; uno por trimestre.</p>
    			<p>Cada dinámica se organiza en un espacio singular que inspira los valores del deporte y la empresa.</p>
    			
    			<p>Si quieres participar en esta experiencia ponte en contacto con nosotros:</p>
    			<p align="center" class="azul"><a href="mailto:empresas@selfcoaching360.com">empresas@selfcoaching360.com</a> - T. 619 351 087</p>
    			
    			
                        <p style="text-align: center;"><a href="https://www.eventbrite.es/e/entradas-desayunando-buenos-cereales-gestion-de-personas-equipos-40788593782" target="_blank" class="descarga">INSCRIBIRME</a></p>
                
                
                <p><strong>Colaboran</strong></p>
                <p>
                    <img src="<?php echo ETG_BASE_URL; ?>/images/cereales-logos.png" alt="Kirolgi - San Sebastián Gipuzkoa Basket" class="img-responsive" />
                </p>


    		</div>
		</div>
		
		
<!--
<div class="eventsWrap">
    			
    			
				<div class="eventItem event-lecture clear">
					<a href="https://www.eventbrite.es/e/entradas-desayunando-buenos-cereales-gestion-de-personas-equipos-40788593782" target="_blank" class="eventItemImg">
						<img src="http://selfcoaching360.com/images/eventos/cereales.jpg" alt="Buenos cereales">
					</a>
					<div class="eventItemDesc">
						<time class="eventItemTime">Martes 14 de Noviembre. Donostia</time>
						<h3><a href="#">Desayunando buenos cereales</a></h3>
						<p>
    					    <strong>Horario:</strong> 09.00h a 11.00h<br>
    					    <strong>Precio:</strong> Sólo con invitación<br>
    					    <strong>Lugar:</strong> GBC. Anoeta Pasealekua, <br />
                        22. Donostia.
    				    </p>
    				    						<a href="https://www.eventbrite.es/e/entradas-desayunando-buenos-cereales-gestion-de-personas-equipos-40788593782" target="_blank" class="eventLearnMore">INSCRIBIRME</a>
    				    					</div>
				</div>
                			</div>



	</section>
-->

    <?php require_once('includes/pie.php'); ?>   
    
</body>
</html>