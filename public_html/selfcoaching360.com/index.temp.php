<?php require_once('includes/config.php'); ?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<title>Self Coaching 360º - Desarrollo personal y liderazgo</title>
		<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
		<!--[if lte IE 8]>
		<script src="<?php echo ETG_BASE_URL; ?>/http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<!--[if lt IE 8]>
			<script src="<?php echo ETG_BASE_URL; ?>/http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
		<![endif]-->
		<link rel="shortcut icon" href="<?php echo ETG_BASE_URL; ?>/favicon.ico" type="image/x-icon" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/bxslider.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/font-awesome.min.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/selectric.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/style.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/adaptive.css" media="screen" />
		
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/jquery.selectric.min.js"></script>
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/jquery.bxslider.min.js"></script>
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/script.js"></script>
	</head>
<body class="home">
    
    <?php 
        $current = 'home';
        require_once('includes/cabecera.php');
    ?>

	<section class="container">
		<div class="homeBxSliderWrap">
			<div class="homeBxSlider">
				<div class="slide active" data-slide="0" style="background-image: url(images/home/slide-01.jpg);">
					<div class="slideDesc">
						<h2>Master Self Coaching 360º</h2>
						<a class="learnMore" href="<?php echo ETG_BASE_URL; ?>/master-360/">Saber más</a>
					</div>
				</div>
				<div class="slide" data-slide="1" style="background-image: url(images/home/slide-02.jpg);">
					<div class="slideDesc">
						<h2>Escuela de desarrollo personal y liderazgo</h2>
						<a class="learnMore" href="<?php echo ETG_BASE_URL; ?>/master-360/">Saber más</a>
					</div>
				</div>
				<div class="slide" data-slide="2" style="background-image: url(images/home/slide-03.jpg);">
					<div class="slideDesc">
						<h2>Un máster de másteres creado para el bienestar de las personas</h2>
						<a class="learnMore" href="<?php echo ETG_BASE_URL; ?>/master-360/">Saber más</a>
					</div>
				</div>
			</div>
		</div>	
		
    <?php require_once('includes/descarga-dossier.php'); ?>
		
		
		
		<div class="homeGrid">
			<div class="mainItem clear"><!-- mainItem mainItemRight clear  - Reverse  -->
				<div class="mainItemImg">
					<img src="<?php echo ETG_BASE_URL; ?>/images/home/home-2.jpg" alt="">
				</div>
				<div class="mainItemDesc">
					<h3>Un máster de másteres</h3>
					<p>Un Máster compuesto de 5 poderosos programas independientes que completan un viaje de 360º en la vida del ser humano.</p>
					<a href="<?php echo ETG_BASE_URL; ?>/master-360/poderosos-programas.php" class="viewMore">Saber más</a>
				</div>
			</div>
			<div class="gridItemWrap clear">
				<a href="<?php echo ETG_BASE_URL; ?>/equipo.php" class="gridItem clear">
					<div class="gridItemImg">
						<img src="<?php echo ETG_BASE_URL; ?>/images/home/silvia-alava-3.jpg" alt="">	
					</div>
					<div class="gridItemDesc">
						<h3>Un equipo de auténtico lujo</h3>
						<p>Eres una persona muy importante para nosotros. Por eso mimamos mucho la calidad y experiencia del equipo de profesionales que te presentamos.</p>
						<span class="viewMore">Saber más 
							<i>
								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" width="9px" height="15px" viewBox="0 0 9 15" enable-background="new 0 0 9 15" xml:space="preserve">
									<path fill="#96e9d5" d="M10-184.5l-0.826 0.757L1.826-177L1-177.758l7.349-6.742L1-191.243L1.826-192l7.349 6.743L10-184.5z M9.174-183.743L9.174-183.743L10-184.5L9.174-183.743z M9.175-185.257L10-184.5v0L9.175-185.257z"/>
									<path fill="#96e9d5" d="M9 7.5L8.174 8.257L0.826 15L0 14.242L7.349 7.5L0 0.757L0.826 0l7.349 6.743L9 7.5z M8.174 8.3 L8.174 8.257L9 7.5L8.174 8.257z M8.175 6.743L9 7.5v0L8.175 6.743z"/>
								</svg>
							</i>
						</span>
					</div>
				</a>
				<a href="<?php echo ETG_BASE_URL; ?>/master-360/informacion.php" class="gridItem gridItemWhite clear">
					<div class="gridItemImg">
						<img src="<?php echo ETG_BASE_URL; ?>/images/home/tabakalera-1.jpg" alt="">	
					</div>
					<div class="gridItemDesc">
						<h3>¿Dónde se imparte?</h3>
						<p>Cada detalle es importante para crear el clima adecuado en cada momento. Los 5 programas se desarrollan en Donostia-San Sebastián.</p>
						<span class="viewMore">Saber más 
							<i>
								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" width="9px" height="15px" viewBox="0 0 9 15" enable-background="new 0 0 9 15" xml:space="preserve">
								<path fill="#96e9d5" d="M10-184.5l-0.826 0.757L1.826-177L1-177.758l7.349-6.742L1-191.243L1.826-192l7.349 6.743L10-184.5z M9.174-183.743L9.174-183.743L10-184.5L9.174-183.743z M9.175-185.257L10-184.5v0L9.175-185.257z"/>
								<path fill="#96e9d5" d="M9 7.5L8.174 8.257L0.826 15L0 14.242L7.349 7.5L0 0.757L0.826 0l7.349 6.743L9 7.5z M8.174 8.3 L8.174 8.257L9 7.5L8.174 8.257z M8.175 6.743L9 7.5v0L8.175 6.743z"/>
								</svg>
							</i>
						</span>
					</div>
				</a>
            </div>
		</div>
    <?php require_once('includes/formulario.php'); ?>
		
	</section>

    <?php require_once('includes/pie.php'); ?>   
    
</body>
</html>