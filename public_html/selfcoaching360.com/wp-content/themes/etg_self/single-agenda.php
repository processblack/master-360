<?php get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	<section class="container">
		<div class="pageHeader" style="background-color: #555; background-image: url(<?php echo  get_the_post_thumbnail_url($post->ID, 'cabecera'); ?>); background-position: center bottom">
			<h1><?php the_title(); ?></h1>
		</div>
		
		
        <?php
        $postID = $post->ID;
        $categorias = get_categories(array('hide_empty' => 0));
        ?>
            
        <div class="pagePanel clear">
            <ul class="productFilter  clear">
            	<?php 
                $args = array(
                    'hide_empty' => 0,
                    'parent' => 0
                );
                $categorias = get_terms('tipo', $args);
                foreach($categorias as $c){
                    ?>
                    <li<?php echo (strtolower(get_query_var( 'term' )) == strtolower($c->name))?' class="current"' : ''; ?>><a href="<?php echo get_term_link($c->term_id); ?>"><?php echo $c->name; ?></a></li>
                    <!-- <li><input type="checkbox" id="<?php echo $c->name; ?>" name="<?php echo $c->slug; ?>"/> <label for="<?php echo $c->name; ?>"><?php echo $c->name; ?></label></li> -->
                    <?php
                }
                ?>
            </ul>
        </div>	
       
		
        	
		<div id="blog">
    		<div class="wrapper">
    			<div class="singlePostWrap">    
            		<?php the_content();?>
            		<p>
					    <strong>Fecha:</strong>   <?php echo get_field('fecha'); ?><br />
					    <strong>Horario:</strong> <?php echo get_field('horario'); ?><br />
					    <strong>Precio:</strong>  <?php echo get_field('precio'); ?><br />
					    <strong>Lugar:</strong>   <?php echo get_field('lugar'); ?>	
				    </p>
				    <?php if (get_field('link_reserva')) { ?>
					<p style="padding: 50px 0;"><a href="<?php echo get_field('link_reserva'); ?>" class="boton" target="_blank">Reservar</a></p>
				    <?php } ?>
    				
                    <div class="social-share">
                        <p><strong>Compartir este evento</strong></p>
                        <a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank" class="ir fb"><img style="opacity: .6;" src="<?php bloginfo( 'template_url' ); ?>/images/iconos/facebook.png" alt="fb" width="24" height="24"></a> 
                        <a href="https://twitter.com/share?url=<?php the_permalink(); ?>&text=<?php echo the_title(); ?> @selfcoaching360" target="_blank" class="ir tw"><img style="opacity: .6;" src="<?php bloginfo( 'template_url' ); ?>/images/iconos/twitter.png" alt="tw" width="24" height="24"></a> 
                        <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&title=<?php the_title(); ?>" target="_blank" class="ir gp"> <img style="opacity: .6;" src="<?php bloginfo( 'template_url' ); ?>/images/iconos/linkedin.png" alt="goo" width="24" height="24"></a>               
    
                    </div>
                </div>
                
                <div id="sidebar">
                    <?php if ( is_active_sidebar( 'sidebar-agenda' ) ) : ?>  
                         <div id="widget-area" class="widget-area">  
                            <?php dynamic_sidebar( 'sidebar-agenda' ); ?>  
                         </div>  
                    <?php endif; ?>
                </div>
            </div>
        </div>
	</section>


<?php endwhile; ?>
<?php get_footer(); ?>