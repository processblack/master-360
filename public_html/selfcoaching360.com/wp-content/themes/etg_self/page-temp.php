<?php 
    // Template Name: Temp
    $GLOBALS['classes_to_body'] = 'single-post';
    get_header();
?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	<section class="container">
		<div class="pageHeader" style="background-color: #555; background-image: url(<?php echo  get_the_post_thumbnail_url($post->ID, 'cabecera'); ?>); background-position: center bottom">
			<h1><?php the_title(); ?></h1>
		</div>
		
        <?php
        $postID = $post->ID;
        $args = array(
            'post_type'      => 'page',
            'posts_per_page' => -1,
            'post_parent'    => $post->post_parent,
            'order'          => 'ASC',
            'orderby'        => 'menu_order'
         );
        
        
        $parent = new WP_Query( $args );
        
        if ( $parent->have_posts() ) : ?>
        <div class="pagePanel clear">
        	<ul class="productFilter  clear">
            	<li id="parent-<?php the_ID(); ?>" class="parent-page primer">
            	    <a href="<?php the_permalink($post->post_parent); ?>"><?php echo get_the_title($post->post_parent); ?></a>
            	</li>
            <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
        
                <li id="parent-<?php the_ID(); ?>" class="parent-page">
                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" <?php if($postID == get_the_ID()){echo ' class="active"'; } ?>><?php the_title(); ?></a>
                    
                </li>
            <?php endwhile; ?>
        	</ul>
        </div>	
        <?php endif; wp_reset_query(); ?>       
		
		
		<?php $content = get_the_content('Read more');
    		if ($content) { ?>
		<div class="wrapper">
			<div class="singlePostWrap">    
        		<?php the_content();?>
            </div>
        </div>
        <?php } ?>







        <div class="classesBox" data-type="parallax" data-speed="10" style="background-color: #c593b4; padding: 60px 20px; height: auto;">
            <a href="#" class="classesCategory">Programa Lanzadera</a>
            <h3>1. AUTOCONOCIMIENTO Y REALIZACIÓN PERSONAL</h3>
        </div>		
		
		<div class="wrapper">
			<div class="singlePostWrap">    
                <p>Autoestima y seguridad personal. Vivir con ilusión y plenitud.</p>
                <ul>
                    <li>Aprenderemos a ser más conscientes.</li>
                    <li>Identificaremos las fases del cambio personal mediante el modelo SDCS. Descubriremos nuestro potencial, nuestras limitaciones y los recursos que pueden ayudarnos a superar los obstáculos que nos encontremos.</li>
                    <li>Conoceremos las claves para asumir las responsabilidades personales.</li>
                    <li>Definiremos un objetivo personal a 91 días y un reto a un año. Y diseñaremos un plan de acción personal para lograrlo.</li>
                    <li>Nos enfocaremos en cómo alcanzar el bienestar personal y nuevos niveles de plenitud. Y descubriremos las herramientas que contribuyen a facilitar, profundizar y acelerar este proceso.</li>
                    <li>Para finalizar, vivirás una última JORNADA PREMIUM OUTDOOR donde pondrás a prueba todo lo aprendido en el programa.</li>
                </ul>
                
                <h3>Técnicas y metodología:</h3>
                
                <p>Modelo SDCS© Selfcoaching.</p>
                
                
                <h3>Equipo de formadores (*):</h3>
                <ul>
                    <li>Iker Fernández <span class="presencial"><span class="ocultar">(</span>Presencial<span class="ocultar">)</span></span></li>
                    <li>Hanna Kanjaa  <span class="presencial"><span class="ocultar">(</span>Webinario<span class="ocultar">)</span></span></li>
                    <li>Pendiente de confirmación</li>
<!--
                    <li>Hana Kanjaa <span class="webinario"><span class="ocultar">(</span>Webinario<span class="ocultar">)</span></span></li>
                    <li>Mónica Fusté <span class="webinario"><span class="ocultar">(</span>Webinario<span class="ocultar">)</span></span></li>
-->
                </ul>
                
                <p>(*) El programa o el equipo pueden sufrir modificaciones por circunstancias ajenas a la Escuela o para cumplir los criterios de calidad del propio Máster.</p>
            </div>
        </div>
        
  
        
        <div class="classesBox" data-type="parallax" data-speed="10" style="background-color: #6caddd; padding: 60px 20px; height: auto;">
            <a href="#" class="classesCategory">Programa</a>
            <h3>2. EDUCACIóN EMOCIONAL Y ENTRENAMIENTO MENTAL</h3>
        </div>		
		
		<div class="wrapper">
			<div class="singlePostWrap">    
                <p>Comprensión y gestión de la mente y nuestras emociones. Alcanzar la consciencia y el equilibrio.</p>
                <ul>
                    <li>Identificaremos los miedos que nos bloquean y saltaremos las barreras que nos impiden alcanzar nuestros propios objetivos.</li>
                    <li>Aprenderemos a gestionar el estrés y alcanzar esa serenidad interior que necesitamos.</li>
                    <li>Sabremos cómo desbloquear las creencias limitantes que adquirimos desde nuestra infancia y conseguiremos identificar y eliminar el sufrimiento.</li>
                    <li>Conseguiremos entender cómo funciona la mente y su influencia en nuestro comportamiento. Descubriremos cómo estimular los pensamientos positivos y ayudar a gestionar los negativos.</li>
                </ul>
                
                <h3>Técnicas y metodología:</h3>
                
                <p>Método Frank Cardelle, Neurociencia, HS Coaching sistémico, Gestalt, Mindfulness.</p>
                
                
                <h3>Equipo de formadores (*):</h3>
                <ul>
                    <li>Luis Oiarzabal <span class="presencial"><span class="ocultar">(</span>Presencial<span class="ocultar">)</span></span></li>
                    <li>Enhamed Enhamed <span class="presencial"><span class="ocultar">(</span>Presencial<span class="ocultar">)</span></span></li>
                    <li>María Soto <span class="presencial"><span class="ocultar">(</span>Presencial<span class="ocultar">)</span></span></li>
                    <li>Ángel de Lope <span class="presencial"><span class="ocultar">(</span>Presencial<span class="ocultar">)</span></span></li>
                    <li>Elena Palomo <span class="presencial"><span class="ocultar">(</span>Presencial<span class="ocultar">)</span></span></li>
                    <li>Pilar Jericó <span class="webinario"><span class="ocultar">(</span>Webinario<span class="ocultar">)</span></span></li>
                    <li>Paula Sopeña <span class="webinario"><span class="ocultar">(</span>Webinario<span class="ocultar">)</span></span></li>
                </ul>
                
            </div>
        </div>
        
        
  
        <div class="classesBox" data-type="parallax" data-speed="10" style="background-color: #f19c9c; padding: 60px 20px; height: auto;">
            <a href="#" class="classesCategory">Programa</a>
            <h3>3. COMUNICACIÓN Y RELACIONES</h3>
        </div>		
		
		<div class="wrapper">
			<div class="singlePostWrap">    
                <p>Nuestro diálogo interno y externo, las relaciones personales, la pareja y la educación de nuestros hijos.</p>
                <ul>
                    <li>Profundizaremos y trabajaremos sobre nuestra comunicación interna para lograr mayor serenidad interior.</li>
                    <li>Descubriremos cuáles son las claves para conectar con otras personas y desarrollar la empatía.</li>
                    <li>Conseguiremos mejorar nuestras habilidades de comunicación y la calidad de nuestras relaciones.</li>
                    <li>Aprenderemos a comunicar con eficacia y a construir relaciones sanas y conscientes.</li>
                    <li>Trabajaremos las relaciones entre padres, madres e hijos y mostraremos el camino para disfrutar de la educación de nuestros hijos.</li>
                    <li>Descubriremos las claves para vivir una relación de pareja enriquecedora. </li>
                </ul>
        
               
                <h3>Técnicas y metodología:</h3>
                
                <p>PNL (Programación Neurolingüística), HS Coaching sistémico, Gestalt, Eneagrama.</p>
                
                
                <h3>Equipo de formadores (*):</h3>
                <ul>
                    <li>Gustavo Bertolotto <span class="presencial"><span class="ocultar">(</span>Presencial<span class="ocultar">)</span></span></li>
                    <li>Pilar Feijoo <span class="presencial"><span class="ocultar">(</span>Presencial<span class="ocultar">)</span></span></li>
                    <li>ángel de Lope <span class="presencial"><span class="ocultar">(</span>Presencial<span class="ocultar">)</span></span></li>
                    <li>Silvia álava <span class="presencial"><span class="ocultar">(</span>Presencial<span class="ocultar">)</span></span></li>
                    <li>Javier Iriondo <span class="webinario"><span class="ocultar">(</span>Webinario<span class="ocultar">)</span></span></li>
                    <li>Pendiente con rmación <span class="webinario"><span class="ocultar">(</span>Webinario<span class="ocultar">)</span></span></li>
                </ul>
                
            </div>
        </div>
        
        
  
        <div class="classesBox" data-type="parallax" data-speed="10" style="background-color: #8ccbc0; padding: 60px 20px; height: auto;">
            <a href="#" class="classesCategory">Programa</a>
            <h3>4. SALUD Y BIENESTAR</h3>
        </div>		
		
		<div class="wrapper">
			<div class="singlePostWrap">    
                <p>El ADN de la salud. Hábitos saludables, nutrición, cuerpo y ejercicio físico. Cultivar la mente. Trascendencia y legado.</p>
                <ul>
                    <li>Descubriremos el ADN de la salud. Profundizaremos sobre dos visiones complementarias de la medicina, la medicina tradicional y la integrativa. Y podrás sacar tus propias conclusiones.</li>
                    <li>Diferenciaremos entre nutrirse y alimentarse. Conoceremos las propiedades de cada alimento y aprenderemos a combinarlos, desarrollaremos nuevos hábitos alimenticios saludables.</li>
                    <li>Aprenderemos a escuchar nuestro cuerpo para sentirnos con más energía cada día y desarrollar una mayor autodisciplina. Trabajaremos la comprensión del cuerpo.</li>
                    <li>Descubriremos cómo lograr bienestar corporal, mental y emocional sostenible en el tiempo.</li>
                    <li>Construiremos un nuevo mapa con buenos hábitos que permitan un cambio definitivo.</li>
                    <li>Viajaremos por los ciclos vitales del ser humano y la trascendencia.</li>
                </ul>
                                
                <h3>Técnicas y metodología:</h3>
                
                <p>Medicina tradicional, Medicina integrativa, Modelo optitud, Pensamiento positivo, Biodanza.</p>
                
                
                <h3>Equipo de formadores (*):</h3>
                <ul>
                    <li>Ander Urruticoechea <span class="presencial"><span class="ocultar">(</span>Presencial<span class="ocultar">)</span></span></li>
                    <li>Karmelo Bizkarra <span class="presencial"><span class="ocultar">(</span>Presencial<span class="ocultar">)</span></span></li>
                    <li>Rakel Ampudia <span class="presencial"><span class="ocultar">(</span>Presencial<span class="ocultar">)</span></span></li>
                    <li>Luis Oiarzabal <span class="presencial"><span class="ocultar">(</span>Presencial<span class="ocultar">)</span></span></li>
                    <li>Aitor Alberdi <span class="presencial"><span class="ocultar">(</span>Presencial<span class="ocultar">)</span></span></li>
                    <li>Gabriela Retana <span class="presencial"><span class="ocultar">(</span>Presencial<span class="ocultar">)</span></span></li>
                    <li>Pilar Feijoo <span class="presencial"><span class="ocultar">(</span>Presencial<span class="ocultar">)</span></span></li>
                    <li>Iosu Lázkoz <span class="presencial"><span class="ocultar">(</span>Presencial<span class="ocultar">)</span></span></li>
                    <li>Pendiente confirmación <span class="webinario"><span class="ocultar">(</span>Webinario<span class="ocultar">)</span></span></li>
                </ul>
                
            </div>
        </div>
        
        
  
        <div class="classesBox" data-type="parallax" data-speed="10" style="background-color: #faca92; padding: 60px 20px; height: auto;">
            <a href="#" class="classesCategory">Programa</a>
            <h3>5. ABUNDANCIA Y RESULTADOS</h3>
        </div>		
		
		<div class="wrapper">
			<div class="singlePostWrap">    
                <p>Vocación y propósito. Realización profesional. Dinero y Finanzas personales. La contribución.</p>
                <ul>
                    <li>Profundizaremos en cómo descubrir nuestra vocación y en dedicar nuestro tiempo a lo que realmente se nos da bien.</li>
                    <li>Identificaremos nuestra pasión y nuestros talentos naturales para explotarlos al máximo.</li>
                    <li>Descubriremos cómo ser más productivo y gestionar mejor nuestro tiempo optimizando nuestros recursos.</li>
                    <li>Aplicaremos técnicas eficaces para mejorar el rendimiento y diseñar un plan de acción alineado con nuestros objetivos.</li>
                    <li>Aprenderemos a identificar y resolver las creencias limitantes que nos alejan del dinero.</li>
                    <li>Conoceremos los medios para alcanzar libertad financiera y vivir en abundancia.</li>
                    <li>Seremos conscientes del gran poder de la contribución y su generosa influencia en nuestra transformación personal. </li>
                </ul>           
                
                <h3>Técnicas y metodología:</h3>
                
                <p>7Ps del liderazgo, 7Ps de la planificación.</p>
                
                
                <h3>Equipo de formadores (*):</h3>
                <ul>
                    <li>Imanol Ibarrondo <span class="presencial"><span class="ocultar">(</span>Presencial<span class="ocultar">)</span></span></li>
                    <li>María Iturriaga <span class="presencial"><span class="ocultar">(</span>Presencial<span class="ocultar">)</span></span></li>
                    <li>José Manuel Torres <span class="presencial"><span class="ocultar">(</span>Presencial<span class="ocultar">)</span></span></li>
                    <li>Claudia Chackelson <span class="presencial"><span class="ocultar">(</span>Presencial<span class="ocultar">)</span></span></li>
                    <li>Iker Fernández <span class="presencial"><span class="ocultar">(</span>Presencial<span class="ocultar">)</span></span></li>
                    <li>Raimon Samsó <span class="webinario"><span class="ocultar">(</span>Webinario<span class="ocultar">)</span></span></li>
                    <li>Pendiente confirmación <span class="webinario"><span class="ocultar">(</span>Webinario<span class="ocultar">)</span></span>.</li>
                </ul>
                
            </div>
        </div>
        
        
  
		<div class="wrapper">
			<div class="singlePostWrap">    
                <p>El programa o el equipo pueden sufrir modificaciones por circunstancias ajenas a la Escuela o para cumplir los criterios de calidad del propio Máster.</p>
            </div>
        </div>
        <?php ETG_contenido(get_the_ID()); ?>
		
	</section>


<?php endwhile; ?>
<?php get_footer(); ?>


