<?php 
    $GLOBALS['classes_to_body'] = 'single-post';
    get_header();
?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); 
    
    if (get_field('alinear_imagen_desacatada')){
        $alinear_imagen_destacada = get_field('alinear_imagen_desacatada');
    } else {
        $alinear_imagen_destacada = 'center bottom';
    }
?>
	<section class="container">
        <?php if (!get_field('ocultar_imagen_de_cabecera')){ ?>
		<div class="pageHeader" style="background-color: #555; background-image: url(<?php echo  get_the_post_thumbnail_url($post->ID, 'cabecera'); ?>); background-position: <?php echo $alinear_imagen_destacada; ?>">
			<h1><?php the_title(); ?></h1>
		</div>
        <?php } ?>
		
       
       
        <?php
        if (!get_field('ocultar_submenu')){
            $postID = $post->ID;
            if ($post->post_parent !== 0){
                $args = array(
                    'post_type'      => 'page',
                    'posts_per_page' => -1,
                    'post_parent'    => $post->post_parent,
                    'order'          => 'ASC',
                    'orderby'        => 'menu_order'
                 );
                
                
                $parent = new WP_Query( $args );
                
                if ( $parent->have_posts() ) : ?>
                <div class="etg_submenu pagePanel clear">
                	<ul class="productFilter  clear">
                    	<li id="parent-<?php the_ID(); ?>" class="parent-page primer">
                    	    <a href="<?php the_permalink($post->post_parent); ?>"><?php echo get_the_title($post->post_parent); ?></a>
                    	</li>
                    <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
                
                        <li id="parent-<?php the_ID(); ?>" class="parent-page">
                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" <?php if($postID == get_the_ID()){echo ' class="active"'; } ?>><?php the_title(); ?></a>
                            
                        </li>
                    <?php endwhile; ?>
                	</ul>
                </div>	
                <?php endif; wp_reset_query();
            }
        } ?>       
       
        
        <?php if (get_field('ocultar_imagen_de_cabecera')){ ?>
		    <div class="pageHeader noimagen">
			<h1><?php the_title(); ?></h1>
			</div>
        <?php } ?>
        	
		<?php  if (get_the_content()) { ?>
		<div class="wrapper">
			<div class="singlePostWrap">    
        		<?php the_content();?>
            </div>
        </div>
        <?php } ?>
		
        <?php ETG_contenido(get_the_ID()); ?>
        <?php #require_once('includes/descarga-dossier.php'); ?>
        <?php #require_once('includes/formulario.php'); ?>
		
	</section>


<?php endwhile; ?>
<?php get_footer(); ?>





