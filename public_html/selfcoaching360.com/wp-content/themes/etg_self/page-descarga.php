<?php 
    // Template Name: Descarga dossier
    #$GLOBALS['inicializarMapa'] = true;
    $GLOBALS['classes_to_body'] = 'single-post';
    get_header();
?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	<section class="container">
		<div class="pageHeader" style="background-color: #555; background-image: url(<?php echo  get_the_post_thumbnail_url($post->ID, 'cabecera'); ?>); background-position: center bottom">
			<h1><?php the_title(); ?></h1>
		</div>
    	
		<div class="wrapper">
			<div class="singlePostWrap"></div>
    		
    		
    		<div id="formulario-dossier">
    			<p class="center"><?php the_content() ?></p>
    			<form id="form-dossi" name="form1" method="post" action="" class="center-block">    			
    				<p class="">
    					<label for="nombre">Nombre</label>
    					<input type="text" name="nombre" id="nombre" />
    				</p>
    				<p class="">
    					<label for="telefono">Teléfono</label>
    					<input type="text" name="telefono" id="telefono" />
    				</p>
    				<p class="">
    					<label for="localidad">Localidad</label>
    					<input type="text" name="localidad" id="localidad" />
    				</p>
    				<p class="">
                        <label for="mail">E-mail</label>
    					<input type="text" name="mail" id="email" />
    				</p>
    				<p>
                    <label>
                        <input type="checkbox" id="checkbox_aceptar" value="si"> Acepto la <a href="<?php the_permalink(268); ?>" target="_blank" style="color: #7f7f7f">política de privacidad</a> del sitio.
                    </label>
    				</p>
    				<div id="resultadoMensaje"></div>
    				<p>
                        <input type="hidden" id="formulario" name="formulario" value="dossier" />
                        <input type="hidden" id="id-dossier" name="id-dossier" value="<?php echo $post->ID; ?>" />
                        <?php $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>
                        <input type="hidden" id="source" name="source" value="<?php echo $actual_link; ?>" />
                        <input type="hidden" id="url" name="url" value="" />
                        <input type="submit" name="enviar" id="enviar_formulario" value="Enviar" class="enviar" />
    				</p>
    			</form>
    		</div>
		</div>
	</section>




<?php endwhile; ?>
<?php get_footer(); ?>


    