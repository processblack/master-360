<?php 
    // Template Name: Eventos
    #$GLOBALS['inicializarMapa'] = true;
    get_header();
?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	<section class="container">
		<div class="pageHeader" style="background-color: #555; background-image: url(<?php echo  get_the_post_thumbnail_url($post->ID, 'cabecera'); ?>); background-position: center bottom">
			<h1><?php the_title(); ?></h1>
		</div>
		
		<?php $content = get_the_content('Read more');
    		if ($content) { ?>
		<div class="wrapper">
			<div class="singlePostWrap">    
        		<?php the_content();?>
            </div>
        </div>
        <?php } ?>
		<div class="contentWrap">
			<div class="pagePanel clear">
				<div class="pageTitle">Eventos</div>
                    <ul class="productFilter  clear">
                    	<?php 
                        $args = array(
                            'hide_empty' => 0,
                            'parent' => 0
                        );
                        $categorias = get_terms('tipo', $args);
                        foreach($categorias as $c){
                            ?>
                            <li<?php echo (strtolower(get_query_var( 'term' )) == strtolower($c->name))?' class="current"' : ''; ?>><a href="<?php echo get_term_link($c->term_id); ?>"><?php echo $c->name; ?></a></li>
                            <!-- <li><input type="checkbox" id="<?php echo $c->name; ?>" name="<?php echo $c->slug; ?>"/> <label for="<?php echo $c->name; ?>"><?php echo $c->name; ?></label></li> -->
                            <?php
                        }
                        ?>
                    </ul>
			</div>
			<div class="eventsWrap">
<?php 
$eventos = array(
#    array(
#        'info_taco' => 'CONFERENCIA: 7 Febrero, SAN SEBASTIÁN',
#        'titulo' => 'Cómo ser tu propio coach en la vida y en el trabajo.',
#        'horario' => '18.30h a 20h',
#        'precio' => 'Entrada abierta y gratuita con previa reserva hasta agotar plazas.',
#        'lugar' => 'Edificio Tabakalera, espacio Impact HUB - planta 3. Andre zigarrogileen Plaza, 1.',
#        'img' => 'conferencia.jpg',
#        'url' => 'https://www.eventbrite.es/e/entradas-como-ser-tu-propio-coach-en-la-vida-y-en-el-trabajo-31581522183'
#    ),
#    array(
#        'info_taco' => 'CONFERENCIA: 9 Febrero, BERGARA',
#        'titulo' => 'Cómo ser tu propio coach en la vida y en el trabajo.',
#        'horario' => '18.30h a 20h',
#        'precio' => 'Entrada abierta y gratuita con previa reserva hasta agotar plazas.',
#        'lugar' => 'Palacio Irizar, 3er. piso. Barrenkalea, 33.',
#        'img' => 'conferencia.jpg',
#        'url' => 'https://www.eventbrite.es/e/entradas-como-ser-tu-propio-coach-en-la-vida-y-en-el-trabajo-31581653576'
#    ),
#    array(
#        'info_taco' => 'CONFERENCIA: 14 Febrero, IRÚN',
#        'titulo' => 'Cómo ser tu propio coach en la vida y en el trabajo.',
#        'horario' => '18.30h a 20h',
#        'precio' => 'Entrada abierta y gratuita con previa reserva hasta agotar plazas',
#        'lugar' => 'CBA – Biblioteca Carlos Blanco Aguinaga. Plaza San Juan, S/N.',
#        'img' => 'conferencia.jpg',
#        'url' => 'https://www.eventbrite.es/e/entradas-como-ser-tu-propio-coach-en-la-vida-y-en-el-trabajo-31581830104'
#    ),
#    array(
#        'info_taco' => 'CONFERENCIA: 16 Febrero, ERMUA (a confirmar)',
#        'titulo' => 'Cómo ser tu propio coach en la vida y en el trabajo.',
#        'horario' => '18.30h a 20h',
#        'precio' => 'Entrada abierta y gratuita con previa reserva hasta agotar plazas',
#        'lugar' => 'A confirmar.',
#        'img' => 'conferencia.jpg',
#        'url' => '#'
#    ),
#    array(
#        'info_taco' => 'CONFERENCIA: 21 Febrero, ERRENTERIA (a confirmar)',
#        'titulo' => 'Cómo ser tu propio coach en la vida y en el trabajo.',
#        'horario' => '18.30h a 20h',
#        'precio' => 'Entrada abierta y gratuita con previa reserva hasta agotar plazas',
#        'lugar' => 'A confirmar.',
#        'img' => 'conferencia.jpg',
#        'url' => '#'
#    ),
#    array(
#        'info_taco' => 'CONFERENCIA: 23 Febrero, ORDIZIA (a confirmar)',
#        'titulo' => 'Cómo ser tu propio coach en la vida y en el trabajo.',
#        'horario' => '18.30h a 20h',
#        'precio' => 'Entrada abierta y gratuita con previa reserva hasta agotar plazas',
#        'lugar' => 'A confirmar.',
#        'img' => 'conferencia.jpg',
#        'url' => '#'
#    ),
#    array(
#        'info_taco' => 'TALLER: 25 Febrero, SAN SEBASTIÁN ',
#        'titulo' => 'Las 3 decisiones: Cómo ser tu propio coach.',
#        'horario' => '9h a 15h',
#        'precio' => '75€ una persona / 110€ dos personas',
#        'lugar' => 'Colegio Mayor Olarain, Ondarreta Pasealekua, 24. ',
#        'img' => 'taller.jpg',
#        'url' => '#'
#    ),
#    array(
#        'info_taco' => 'CONFERENCIA: 1 Marzo, SAN SEBASTIÁN ',
#        'titulo' => 'Cómo ser tu propio coach en la vida y en el trabajo.',
#        'horario' => '18.30h a 20h',
#        'precio' => 'Entrada abierta y gratuita con previa reserva hasta agotar plazas',
#        'lugar' => 'A confirmar.',
#        'img' => 'conferencia.jpg',
#        'url' => '#'
#    ),


#    array(
#        'info_taco' => 'CONFERENCIA: 8 Marzo, PAMPLONA',
#        'titulo' => 'Cómo ser tu propio coach en la vida y en el trabajo.',
#        'horario' => '18.30h a 20h',
#        'precio' => 'Gratuito. Acceso con inscripción.',
#        'lugar' => 'Abba Hotel Reino de Navarra<br />C/ de Acella 1, Pamplona',
#        'img' => 'conferencia.jpg',
#        'url' => 'https://www.eventbrite.es/e/entradas-conferencia-como-ser-tu-propio-coach-en-la-vida-y-en-el-trabajo-32346619610'
#    ),
#    array(
#        'info_taco' => 'JORNADA DE PUERTAS ABIERTAS: 9 Marzo, SAN SEBASTIÁN',
#        'titulo' => 'Presentación del máster de desarrollo personal Selfcoaching 360.',
#        'horario' => '18.30h a 20h',
#        'precio' => 'Gratuito. Acceso con inscripción.',
#        'lugar' => 'TABAKALERA, Espacio Impact HUB, Planta<br />Paseo Duque de Mandas 52, San Sebastián',
#        'img' => 'puertas-abiertas.jpg',
#        'url' => 'https://www.eventbrite.es/e/entradas-jornada-de-puertas-abiertasmaster-de-desarrollo-personal-selfcoaching-360o-31581949461'
#    ),
#    array(
#        'info_taco' => 'TALLER: 11 Marzo, SAN SEBASTIÁN',
#        'titulo' => 'Taller las 3 decisiones.',
#        'horario' => '9h a 15h',
#        'precio' => '80€ persona / 120€ dos personas',
#        'lugar' => 'Colegio Mayor Olarain<br />Ondarreta Pasealekua 24, Donostia',
#        'img' => 'taller.jpg',
#        'url' => 'https://www.eventbrite.es/e/entradas-taller-de-las-3-decisiones-32346509280'
#    ),
#	
#    array(
#        'info_taco' => 'CONFERENCIA: 16 Marzo, VITORIA',
#        'titulo' => 'Cómo ser tu propio coach en la vida y en el trabajo.',
#        'horario' => '19h a 20.30h',
#        'precio' => 'Gratuito. Acceso con inscripción.',
#        'lugar' => 'Urkide Ikastetxea<br />C/Magdalena 8, Vitoria',
#        'img' => 'conferencia.jpg',
#        'url' => 'https://www.eventbrite.es/e/entradas-conferencia-como-ser-tu-propio-coach-en-la-vida-y-en-el-trabajo-32346532349'
#    ),
#    array(
#        'info_taco' => 'JORNADA DE PUERTAS ABIERTAS: 22 Marzo, PAMPLONA',
#        'titulo' => 'Presentación del máster de desarrollo personal Selfcoaching 360.',
#        'horario' => '18.30h a 20h',
#        'precio' => 'Gratuito. Acceso con inscripción.',
#        'lugar' => 'Abba hotel Reino de Navarra<br />C/ de Acella 1, Pamplona',
#        'img' => 'puertas-abiertas.jpg',
#        'url' => 'https://www.eventbrite.es/e/entradas-jornada-de-puertas-abiertasmaster-de-desarrollo-personal-selfcoaching-360o-32346678787'
#    ),
#    array(
#        'info_taco' => 'CONFERENCIA: 28 Marzo, BILBAO',
#        'titulo' => 'Cómo ser tu propio coach en la vida y en el trabajo.',
#        'horario' => '18.30h a 20h',
#        'precio' => 'Gratuito. Acceso con inscripción.',
#        'lugar' => 'BIZKAIA ARETOA UPV/EHU, SALA OTEIZA<br />Avenida Abandoibarra 3, Bilbao',
#        'img' => 'conferencia.jpg',
#        'url' => 'https://www.eventbrite.es/e/entradas-conferencia-como-ser-tu-propio-coach-en-la-vida-y-en-el-trabajo-32346666751'
#    ),
#    array(
#        'info_taco' => 'CONFERENCIA: 29 Marzo, Pasajes San Pedro',
#        'titulo' => 'Cómo ser tu propio coach en la vida y en el trabajo.',
#        'horario' => '18.30h a 20h',
#        'precio' => 'Gratuito. Acceso con inscripción.',
#        'lugar' => 'ALBAOLA ITSAS KULTUR FAKTORIA<br />	Ondartxo 1, Pasajes San Pedro',
#        'img' => 'conferencia.jpg',
#        'url' => 'https://www.eventbrite.es/e/entradas-conferencia-como-ser-tu-propio-coach-en-la-vida-y-en-el-trabajo-32321664970'
#    ),
#    array(
#        'info_taco' => 'JORNADA DE PUERTAS ABIERTAS: 30 Marzo, VITORIA',
#        'titulo' => 'Presentación del máster de desarrollo personal Selfcoaching 360.',
#        'horario' => '18.30h a 20h',
#        'precio' => 'Gratuito. Acceso con inscripción.',
#        'lugar' => 'URKIDE IKASTETXEA<br />C/Magdalena 8, Vitoria',
#        'img' => 'puertas-abiertas.jpg',
#        'url' => 'https://www.eventbrite.es/e/entradas-jornada-de-puertas-abiertasmaster-de-desarrollo-personal-selfcoaching-360o-32346651706'
#    ),
#	
#    array(
#        'info_taco' => 'JORNADA DE PUERTAS ABIERTAS: 5 Abril, BILBAO',
#        'titulo' => 'Presentación del máster de desarrollo personal Selfcoaching 360.',
#        'horario' => '18.30h a 20h',
#        'precio' => 'Gratuito. Acceso con inscripción.',
#        'lugar' => 'BIZKAIA ARETOA UPV/EHU<br />Abandoibarra 3, Bilbao',
#        'img' => 'puertas-abiertas.jpg',
#        'url' => 'https://www.eventbrite.es/e/entradas-jornada-de-puertas-abiertasmaster-de-desarrollo-personal-selfcoaching-360o-32346691826'
#    ),
    array(
        'info_taco' => 'Conferencia: 2 Mayo, ORDIZIA',
        'titulo' => 'Cómo ser tu propio coach en la vida y en el trabajo.',
        'horario' => '9h a 15h',
        'precio' => 'Gratuito. Acceso con inscripción.',
        'lugar' => 'D\'Elikatuz Santamaria-Andre Mari Kalea, 24 20240 Ordizia',
        'img' => 'conferencia.jpg',
        'url' => 'https://www.eventbrite.es/e/entradas-conferencia-como-ser-tu-propio-coach-en-la-vida-y-en-el-trabajo-33302785528'
    ),
    array(
        'info_taco' => 'TALLER: 13 Mayo, PAMPLONA',
        'titulo' => 'Taller las 3 decisiones.',
        'horario' => '9h a 15h',
        'precio' => '80€ persona / 120€ dos personas',
        'lugar' => 'ABBA HOTEL REINO DE NAVARRA<br />C/ de Acella 1, Pamplona',
        'img' => 'taller.jpg',
        'url' => 'https://www.eventbrite.es/e/entradas-taller-de-las-3-decisiones-32321811408'
    ),
    array(
        'info_taco' => 'TALLER: 20 Mayo, BILBAO',
        'titulo' => 'Taller las 3 decisiones.',
        'horario' => '9h a 15h',
        'precio' => '80€ persona / 120€ dos personas',
        'lugar' => 'ESPACIO ARBAT-BILBAO<br />Ribera de Botica Vieja 21, Bilbao',
        'img' => 'taller.jpg',
        'url' => 'https://www.eventbrite.es/e/entradas-taller-de-las-3-decisiones-32321747216'
    )

);

                foreach ($eventos as $evento) {         
?>    			
    			
				<div class="eventItem event-lecture clear">
					<a href="single-event.html" class="eventItemImg">
						<img src="/images/eventos/<?php echo $evento['img']; ?>" alt="<?php echo $evento['titulo']; ?>" />
					</a>
					<div class="eventItemDesc">
						<time class="eventItemTime"><?php echo $evento['info_taco']; ?></time>
						<h3><a href="#"><?php echo $evento['titulo']; ?></a></h3>
						<p>
    					    <strong>Horario:</strong> <?php echo $evento['horario']; ?><br />
    					    <strong>Precio:</strong> <?php echo $evento['precio']; ?><br />
    					    <strong>Lugar:</strong> <?php echo $evento['lugar']; ?>	
    				    </p>
    				    <?php if ($evento['url'] !== '#') { ?>
						<a href="<?php echo $evento['url']; ?>" class="eventLearnMore">Reservar</a>
    				    <?php } ?>
					</div>
				</div>
                <?php } ?>
			</div>
		</div>
        
		
	</section>


<?php endwhile; ?>
<?php get_footer(); ?>


