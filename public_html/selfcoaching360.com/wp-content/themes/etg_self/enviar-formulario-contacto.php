<?php 
    include('../../../wp-load.php');       
    if ($_POST['url']){
        die();
    } else {
        $campo_de_interes  = $_POST['campo_de_interes'];
        $formulario  = $_POST['formulario'];
        if ($_POST['id_dossier']){
            $id_dossier  = $_POST['id_dossier'];
        } else {
            $id_dossier  = 0;
        }
        $aceptar     = $_POST['aceptar'];
        $nombre      = $_POST['nombre'];
        $email       = $_POST['email'];
        if (isset($_POST['source'])) {
            $source      = $_POST['source'];
        } else {
            $source      = '-';
        }
        $telefono    = $_POST['telefono'];
        $localidad   = $_POST['localidad'];
        $enviar_a    = str_replace('ARROBA', '@', $_POST['enviar_a']);
        
        
        $campo_de_interes = str_replace( 'si |', '', $campo_de_interes);
            
        
        if ($aceptar == 'si') {
            #validamos el email
            if (!preg_match("/^[A-Z0-9._%-]+@[A-Z0-9.-]+.[A-Z]{2,4}$/i",$email)){
                $jason = array('validate'=>FALSE, 'mensaje' => 'El email no es válido');
                echo (json_encode($jason));
            } else {
            	// compara fechas
                # Miramos si la fecha de reserva es lunes
                if ($nombre !== "" and $email !== "" and $localidad !== "" and $telefono !== ""){
                    //para el envío en formato HTML 
                    $headers = "MIME-Version: 1.0\r\n"; 
                    $headers .= "Content-type: text/html; charset=iso-8859-1\r\n"; 
                    #$headers .= "CC: miguel@eltipografico.com" . "\r\n";
                    
                    //dirección del remitente 
                    $headersCliente = $headers."From: ".utf8_decode($nombre)." <".utf8_decode($email).">\r\n"; 
                    $headers .= "From: ".utf8_decode($nombre)." <".utf8_decode($email).">\r\n"; 
                    
                    
                    $body = "<ul>";
                    $body .= "<li><strong>Nombre</strong>: ".$nombre."</li>";
                    $body .= "<li><strong>Email</strong>: ".$email."</li>";
                    $body .= "<li><strong>Teléfono</strong>: ".$telefono."</li>";
                    $body .= "<li><strong>Localidad</strong>: ".$localidad."</li>";
                    $body .= "<li><strong>Campo de interés</strong>: ".$campo_de_interes."</li>";
                    $body .= "<li><strong>URL</strong>: ".$source."</li>";
                    $body .= "</ul>";
                    
                    
                    if (get_field('mensaje_para_el_usuario', $id_dossier)){
                        $bodyCliente = get_field('mensaje_para_el_usuario', $id_dossier);
                    }                    
                    
                    if ($formulario == 'dossier'){
                        switch($id_dossier){
                            # Máster
                            case '245':
                                $asunto = '[SELFCOACHING 360] '.get_the_title($id_dossier);
                                $campo_de_interes = 'Máster Selfcoaching 360';
                            break;
                            case '2442':
                                $asunto = '[SELFCOACHING 360] '.get_the_title($id_dossier);
                                $campo_de_interes = 'Programa ejecutivo Selfcoaching';
                            break;
                            case '245':
                                $asunto = '[SELFCOACHING 360] '.get_the_title($id_dossier);
                                $campo_de_interes = get_the_title($id_dossier);
                            break;
                        }
                    } else {
                        $asunto = '[SELFCOACHING 360] Interesado en: '.$campo_de_interes;
                    }
                    
                    $enviar_a = 'info@selfcoaching360.com';
/*
                    if (mail('miguel@eltipografico.com', $asunto, utf8_decode($body), $headers)){
                        if ($formulario == 'dossier'){
                            mail($email, "[SELFCOACHING 360] Descarga Dossier Selfcoaching 360.", utf8_decode($bodyCliente), $headersCliente);
                        }
                        $jason = array('validate'=>TRUE);
                    } else {
                        $jason = array('validate'=>FALSE, 'mensaje' => '');
                    }
*/
                    
                                        
                    require_once('includes/class.phpmailer.php');
                    
                    // Configuración básica del envío
                    $email_host     = "smtp.eltipografico.com";
                    $email_username = "webmailer@eltipografico.com";
                    $email_password = "navarrote69";
                    
                	$html_cabecera = '<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /></head><body>';
                	$html_cabecera .= '</head><body>';

                	$html_pie = '</body></html>';
                    
                    //include("class.smtp.php"); // optional, gets called from within class.phpmailer.php if not already loaded
                    
                    $mail = new PHPMailer(true);
                    #$mail->IsSMTP(); // telling the class to use SMTP
                    $mail->Host       = $email_host;     // SMTP server
                    $mail->SMTPDebug  = 2;               // enables SMTP debug information (for testing)
                    $mail->SMTPAuth   = true;            // enable SMTP authentication
                    $mail->Port       = 25;              // set the SMTP port for the GMAIL server
                    $mail->Username   = $email_username; // SMTP account username
                    $mail->Password   = $email_password; // SMTP account password
                    
                    $mail->AddAddress($enviar_a);
/*
                    if (strpos($campo_de_interes, 'Entre girasoles y viñedos') !== false) {
                        $mail->AddAddress('iker@hacedordeaventuras.com', 'Iker');
                    }
                    if (strpos($campo_de_interes, 'Buenos cereales') !== false) {
                        $mail->AddAddress('empresa@sellcoaching360.com', 'Selfcoaching Empresas');
                    }
*/
                    $mail->AddBCC('miguel@eltipografico.com', 'Miguel');
                    $mail->SetFrom($email, $nombre);
                    //$mail->AddReplyTo($email);
                    $mail->Subject = $asunto;
                    
                    $html = $html_cabecera.$body.$html_pie;
                    
                    #die($html);
                    $mail->MsgHTML($html);
                    if (!$mail->Send()){
                        $jason = array('validate'=>FALSE, 'mensaje' => '');
                        die();
                    }
                    
                    # Envío al usuario
                    if (isset($bodyCliente)){
                        
                        $html = $html_cabecera.$bodyCliente.$html_pie;
                        
                        $mail_cliente = new PHPMailer(true);
                        #$mail->IsSMTP(); // telling the class to use SMTP
                        $mail_cliente->Host       = $email_host;     // SMTP server
                        $mail_cliente->SMTPDebug  = 2;               // enables SMTP debug information (for testing)
                        $mail_cliente->SMTPAuth   = true;            // enable SMTP authentication
                        $mail_cliente->Port       = 25;              // set the SMTP port for the GMAIL server
                        $mail_cliente->Username   = $email_username; // SMTP account username
                        $mail_cliente->Password   = $email_password; // SMTP account password
                        
                        $mail_cliente->AddAddress($email);
                        $mail_cliente->AddBCC('miguel@eltipografico.com', 'Miguel');
                        $mail_cliente->SetFrom('master@selfcoaching360.com', 'Selfcoaching 360');
                        $mail_cliente->Subject = $asunto;
                        //$mail_cliente->AddReplyTo($para);
                        $mail_cliente->MsgHTML($html);
                        if (!$mail_cliente->Send()){
                            $jason = array('validate'=>FALSE, 'mensaje' => '');
                            die();
                        }
                    }
                    
                        
                    $jason = array('validate'=>TRUE);   
                    
                    
                    
                    // =============================================
                    // CONFIGURATIONS
                    // =============================================
                    
                    // Authentication
                    $api_key         = '1e58418d6c031a38db3aaf2bec06dd95-us10'; // Find on your Account Settings > Extras > API Keys
                    $list_id         = 'ccf26fed0c'; // Find on your List > Settings
                    
                    // Validation messages
                    $error_messages   = array(
                    	'List_AlreadySubscribed' => 'The email you entered is already subscribed.',
                    	'Email_NotExists'        => 'The email you entered is invalid.',
                    	'else'                   => 'An error occurred.',
                    );
                    $success_message = 'Success! Please check your e-mail to complete the subscription.';
                    
                    // =============================================
                    // BEGIN SUBSCRIBE PROCESS
                    // =============================================
                    
                    // Form's values
                    $email = isset( $_REQUEST['email'] ) ? $_REQUEST['email'] : '';
                    
                    // Initiate API object
                    require_once( '../../../php/mailchimp/class.mailchimp-api.php' );
                    $mailchimp = new MailChimp( $api_key );
                    
                    // Request parameters
                    $config  = array(
                    	'id'                => $list_id,
                    	'email'             => array( 'email' => $email ),
                    	'merge_vars'        => array('PHONE' => $telefono, 'FNAME' => $nombre, 'LOCALIDAD' => $localidad, 'INTERES' => $campo_de_interes),
                    	'merge_fields'      => array('PHONE' => $telefono, 'FNAME' => $nombre, 'LOCALIDAD' => $localidad, 'INTERES' => $campo_de_interes),
                    	'email_type'        => 'html',
                    	'double_optin'      => false,
                    	'update_existing'   => false,
                    	'replace_interests' => true,
                    	'send_welcome'      => false,
                    );
                    
                    // Send request
                    // http://apidocs.mailchimp.com/api/2.0/lists/subscribe.php
                    $result = $mailchimp->call( 'lists/subscribe', $config );
                    
                    if ( array_key_exists( 'status', $result ) && $result['status'] == 'error' ) {
                    	// If error occurs
                    	$result['message'] = array_key_exists( $result['name'], $error_messages ) ? $error_messages[ $result['name'] ] : $error_messages['else'];
                    } else {
                    	// If success
                    	$result['message'] = $success_message;
                    }
                    
                    // Send output
                    if ( ! empty( $_REQUEST[ 'ajax' ] ) ) {
                    	// called via AJAX
                    	echo json_encode( $result );
                    } else {
                    	// no AJAX
                    	if ( array_key_exists( 'status', $result ) && $result['status'] == 'error' ) {
                    		#echo 'Error: ' . $result['error'];
                    	}/*
 else {
                    		echo 'Success';
                    	}
*/
                    }                    
                                        
                    
                    
                             
                    echo (json_encode($jason));
                    
                    
                } else {
                    $jason = array('validate'=>FALSE, 'mensaje' => 'Todos los campos son obligatorios');
                    echo (json_encode($jason));
                }
            }
        } else {
            $jason = array('validate'=>FALSE, 'mensaje' => 'Tienes que aceptar la política de privacidad del sitio.');
            echo (json_encode($jason));
        }
        
        
    }            
