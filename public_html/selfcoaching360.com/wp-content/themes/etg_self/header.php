<!doctype html>
<html>
	<head>
	  	<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>
		<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
		<!--[if lte IE 8]>
		<script src="<?php bloginfo( 'template_url' ); ?>/http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<!--[if lt IE 8]>
			<script src="<?php bloginfo( 'template_url' ); ?>/http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
		<![endif]-->
		<link rel="shortcut icon" href="<?php bloginfo( 'template_url' ); ?>/favicon.ico" type="image/x-icon" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo( 'template_url' ); ?>/css/bxslider.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo( 'template_url' ); ?>/css/font-awesome.min.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo( 'template_url' ); ?>/css/selectric.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo( 'template_url' ); ?>/style.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo( 'template_url' ); ?>/css/adaptive.css" media="screen" />
		
<!--
		<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/js/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/js/jquery.selectric.min.js"></script>
		<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/js/jquery.bxslider.min.js"></script>
		<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/js/script.js"></script>
-->
		
		
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

		<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>


		<?php wp_head(); ?>

	<?php 
    	
    if (isset($GLOBALS['classes_to_body'])){
        $classes_to_body = $GLOBALS['classes_to_body'].' single-post';
    } else {
        $classes_to_body = 'single-post';
    }
    	
    	
    if (isset($GLOBALS['inicializarMapa'])) { ?>
	    <script type="text/javascript" src="//maps.google.com/maps/api/js?sensor=true"></script>
	    </head>	
	    <body <?php body_class($classes_to_body); ?> onload="initialize()">
	<?php } else { ?>
	    </head>	
	    <body <?php body_class($classes_to_body); ?>>
	<?php } ?>	
        

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-89756104-2', 'auto');
  ga('send', 'pageview');

</script>	
	
	
	<header id="header">
		<div class="headerWrap clear">
			<a href="/" class="logo">
				<!-- You can also use image as a logo the example below -->
				<img class="logo-white" src="<?php bloginfo( 'template_url' ); ?>/images/coaching-logo-white.png" srcset="<?php bloginfo( 'template_url' ); ?>/images/coaching-logo-white@2x.png 2x" alt="Self Coaching 360" />
				<img class="logo-black" src="<?php bloginfo( 'template_url' ); ?>/images/coaching-logo.png" srcset="<?php bloginfo( 'template_url' ); ?>/images/coaching-logo@2x.png 2x" alt="Self Coaching 360" />
			</a>
			<nav class="mainMenu">
                <?php wp_nav_menu( array('menu' => 'Principal', 'container' => false, 'menu_class' => 'text-lowercase' )); ?>
			</nav>
			<span class="showMobileMenu">
				<span></span>
				<span></span>
				<span></span>
				<span></span>
			</span>
		</div>
	</header>