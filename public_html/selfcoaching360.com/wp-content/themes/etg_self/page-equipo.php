<?php 
    // Template Name: Equipo
    #$GLOBALS['inicializarMapa'] = true;
    $GLOBALS['classes_to_body'] = 'single-post';
    get_header();
?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>


	<section class="container">
		
    	
		<div class="ourTeam">
			<div class="teamItemWrap clear">

				<?php 
    				$i = 1;
                    if( have_rows('miembros') ):
                        while ( have_rows('miembros') ) : the_row();
                            ?>
                            <div data-userid="user_<?php echo $i; ?>" class="teamItem">
            					<?php $foto = get_sub_field('foto'); ?>
            					<?php $foto_azul = get_sub_field('foto_azul'); ?>
            					<img src="<?php echo $foto_azul['url']; ?>" alt="<?php echo the_sub_field('nombre'); ?>">
            					<div class="overlay">
            						<div class="teamItemNameWrap">
            							<h3><?php echo the_sub_field('nombre'); ?></h3>
            						</div>
            						<p><?php echo the_sub_field('titulo'); ?></p>
            					</div>
            				</div>	
            
            				
            				<div id="user_<?php echo $i; ?>" class="teamItemDesc">
            					<div class="teamItemDescWrap">
            						<img src="<?php echo $foto['url']; ?>" alt="<?php echo the_sub_field('nombre'); ?>" class="teamItemImg img-responsive center-block">
            						<h3><?php echo the_sub_field('nombre'); ?></h3>
            						<p class="teamItemDescText1"><?php echo the_sub_field('titulo'); ?></p>
            						<p class="teamItemDescText"><?php echo the_sub_field('bio'); ?></p>
            <!--
            						<div class="teamItemSocial">
            							<a href="#"><i class="fa fa-facebook"></i></a>
            							<a href="#"><i class="fa fa-twitter"></i></a>
            							<a href="#"><i class="fa fa-pinterest"></i></a>
            						</div>
            -->
            					</div>
            					<span class="closeTeamDesc">
            						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" width="16px" height="16px" viewBox="1.5 -1 16 16" enable-background="new 1.5 -1 16 16" xml:space="preserve">
            							<path fill="#C1F4E8" d="M11.185 7l6.315 6.314L15.814 15L9.5 8.685L3.186 15L1.5 13.314L7.815 7L1.5 0.686L3.186-1L9.5 5.3 L15.814-1L17.5 0.686L11.185 7z"/>
            						</svg>
            					</span>
            				</div>				
            				<?php 
                				$i++; 

                            
                    
                        endwhile;
                    endif;
 ?>

				
            </div>
        </div>
        
		<div class="wrapper">
			<div class="singlePostWrap">    
    			<?php the_content(); ?>
    		</div>
		</div>
		
        
		
        <?php ETG_contenido(get_the_ID()); ?>
        <?php #require_once('includes/descarga-dossier.php'); ?>
        <?php #require_once('includes/formulario.php'); ?>
	</section>
<?php endwhile; ?>
<?php get_footer(); ?>
