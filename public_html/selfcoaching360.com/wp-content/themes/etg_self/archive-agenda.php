<?php 
    // Template Name: Eventos
    #$GLOBALS['inicializarMapa'] = true;
    $GLOBALS['classes_to_body'] = 'events';
    get_header();
?>
	<section class="container">
		<div class="pageHeader" style="background-color: #555; background-image: url(http://selfcoaching360.com/images/eventos.jpg); background-position: center bottom">
			<h1>SIGUE NUESTROS EVENTOS</h1>
		</div>
		
		<div class="contentWrap">
			<div class="pagePanel clear">
				<div class="pageTitle">Eventos</div>
                    <ul class="productFilter  clear">
                    	<?php 
                        $args = array(
                            'hide_empty' => 0,
                            'parent' => 0,
                            'exclude' => array(64)
                        );
                        $categorias = get_terms('tipo', $args);
                        foreach($categorias as $c){
                            ?>
                            <li<?php echo (strtolower(get_query_var( 'term' )) == strtolower($c->name))?' class="current"' : ''; ?>><a href="<?php echo get_term_link($c->term_id); ?>"><?php echo $c->name; ?></a></li>
                            <!-- <li><input type="checkbox" id="<?php echo $c->name; ?>" name="<?php echo $c->slug; ?>"/> <label for="<?php echo $c->name; ?>"><?php echo $c->name; ?></label></li> -->
                            <?php
                        }
                        ?>
                    </ul>
			</div>
			<div class="wrapper">
                <?php
                    
        		$la_fecha = date('Y-m-d');
                $sqlAgenda = "SELECT * FROM (select * from wp_fechas where (fecha >= '".$la_fecha."') and (idioma = '".IDIOMA_AGENDA."') order by fecha asc) as temp_table group by idEvento order by fecha asc;";
                    
        		global $wpdb;
        		$resultados = $wpdb->get_results( $sqlAgenda, ARRAY_A );  
        		set_query_var('resultados', $resultados); # pasa los valores
                    

                require_once('loop-agenda.php');

                ?>
			</div>
		</div>
        
    <?php 
        get_template_part('contenidos/formulario');
        get_template_part('contenidos/redes_sociales');
    ?>
	</section>
<?php get_footer(); ?>


