	<footer id="footer" class="clear">
		<div class="footerSocial clear">
            <p style="text-align: center; padding: 30px 0;">
                <a href="https://www.facebook.com/selfcoaching360/" target="_blank"><img src="<?php bloginfo( 'template_url' ); ?>/images/iconos/facebook.png" srcset="<?php bloginfo( 'template_url' ); ?>/images/iconos/facebook@2x.png 2x" alt="Facebook" /></a>
                <a href="http://www.instagram.com/selfcoaching360" target="_blank"><img src="<?php bloginfo( 'template_url' ); ?>/images/iconos/instagram.png" srcset="<?php bloginfo( 'template_url' ); ?>/images/iconos/instagram@2x.png 2x" alt="instagram" /></a>
                <a href="https://twitter.com/selfcoaching360" target="_blank"><img src="<?php bloginfo( 'template_url' ); ?>/images/iconos/twitter.png" srcset="<?php bloginfo( 'template_url' ); ?>/images/iconos/twitter@2x.png 2x" alt="twitter" /></a>
                <a href="http://www.youtube.com/user/iflecuona" target="_blank"><img src="<?php bloginfo( 'template_url' ); ?>/images/iconos/youtube.png" srcset="<?php bloginfo( 'template_url' ); ?>/images/iconos/youtube@2x.png 2x" alt="youtube" /></a>
                <a href="http://www.linkedin.com/company-beta/11062408" target="_blank"><img src="<?php bloginfo( 'template_url' ); ?>/images/iconos/linkedin.png" srcset="<?php bloginfo( 'template_url' ); ?>/images/iconos/linkedin@2x.png 2x" alt="linkedin" /></a>
                <a href="https://www.ivoox.com/escuchar-escuela-selfcoaching-360_nq_321561_1.html" target="_blank"><img src="<?php bloginfo( 'template_url' ); ?>/images/iconos/ivoox.png" alt="ivoox" /></a>
            </p>
		</div>
		
                <?php wp_nav_menu( array('menu' => 'PIe', 'container' => false, 'menu_class' => 'text-uppercase footerMenu clear' )); ?>
		
<!--
		<div class="footerSubscribe">
			<form>
				<input class="" type="text" name="" size="20" value="" placeholder="Your email">
				<input class="btnSubscribe" type="submit" value="Subscribe">
			</form>
		</div>
-->
		<div class="copyright">
			<p>2017. Máster Self Coaching 360º.<br />
			
			<a href="/politica-de-cookies">Política de privacidad</a> - 
			<a href="/aviso-legal">Aviso Legal</a> - 
			<a href="/politica-de-cookies">Política de cookies</a>
			
			</p>
		</div>
	</footer>
	<div class="mobileMenu">
        <?php wp_nav_menu( array('menu' => 'Principal', 'container' => false, 'menu_class' => 'text-lowercase' )); ?>
	</div>

<?php wp_footer(); ?>

</body>
</html>