<?php 
    // Template Name: Muestra páginas hijo
    get_header();
?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>


        <?php
        $postID = $post->ID;
        $args = array(
            'post_type'      => 'page',
            'posts_per_page' => -1,
            'post_parent'    => $post->ID,
            'order'          => 'ASC',
            'orderby'        => 'menu_order'
         );
        
        
        $parent = new WP_Query( $args );
        
        if ( $parent->have_posts() ) : ?>
        <div class="etg_submenu pagePanel clear">
        	<ul class="productFilter  clear">
            	<li id="parent-<?php the_ID(); ?>" class="parent-page primer">
            	    <a href="<?php the_permalink($postID); ?>"><?php echo get_the_title($postID); ?></a>
            	</li>
            <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
        
                <li id="parent-<?php the_ID(); ?>" class="parent-page">
                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" <?php if($postID == get_the_ID()){echo ' class="active"'; } ?>><?php the_title(); ?></a>
                    
                </li>
            <?php endwhile; ?>
        	</ul>
        </div>	
        <?php endif; wp_reset_query(); ?>       


		<?php  if (get_the_content()) { ?>
		<div class="wrapper">
			<div class="singlePostWrap">    
        		<?php the_content();?>
            </div>
        </div>
        <?php } ?>


	<section class="container">
<!--
		<div class="blogPostWrap">
    		
            <?php
            $paginas = get_pages( array( 'child_of' => $post->ID, 'parent' => $post->ID, 'sort_order' => 'ASC', 'sort_column' => 'ID'));
            
            foreach( $paginas as $pagina ) {
            ?>
			<div class="postItem">
				<a href="<?php echo get_page_link( $pagina->ID ); ?>" class="postItemImg">
					<?php 
    				if (has_post_thumbnail($pagina->ID)) {
					    echo get_the_post_thumbnail($pagina->ID, 'poster');
					} else {
    				    echo "no tiene..";
    				} ?>
				</a>
				<h4><a href="<?php echo get_page_link( $pagina->ID ); ?>"><?php echo $pagina->post_title; ?></a></h4>
				<?php if (get_field('subtitulo', $pagina->ID)){ ?>
				<p><?php echo get_field('subtitulo', $pagina->ID); ?></p>
				<?php } ?>
			</div>
            <?php
            }	
            ?>
        </div>
        
-->
        
        
        <?php ETG_contenido(get_the_ID()); ?>
        <?php #require_once('includes/descarga-dossier.php'); ?>
        <?php #require_once('includes/formulario.php'); ?>
	</section>
<?php endwhile; ?>
<?php get_footer(); ?>
