<?php 
    // Template Name: Página de contacto
    #$GLOBALS['inicializarMapa'] = true;
    get_header();
?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>


	<section class="container">
		<div class="pageHeader" style="background-color: #555; background-image: url(<?php echo  get_the_post_thumbnail_url($post->ID, 'cabecera'); ?>); background-position: center bottom">
			<h1><?php the_title(); ?></h1>
		</div>


<div class="wrapper clear">
	<div class="contactGallery">
		
        <form class="formulario">
          <div class="form-group clearfix">
					<label for="nombre">Nombre</label>
					<input type="text" name="nombre" id="nombre" />
          </div>
          <div class="form-group clearfix">
					<label for="telefono">Teléfono</label>
					<input type="text" name="telefono" id="telefono" />
          </div>
          <div class="form-group clearfix">
					<label for="localidad">Localidad</label>
					<input type="text" name="localidad" id="localidad" />
          </div>
          <div class="form-group clearfix">
                    <label for="email">E-mail</label>
					<input type="text" name="email" id="email" />
          </div>
          
          
          <div class="form-group clearfix">
            <label for="comentario" class="col-sm-2 control-label">Comentario</label>
              <textarea name="comentario" id="comentario" rows="9" cols="9"></textarea>
          </div>
				<p style="clear: both">
                <label>
                    <input type="checkbox" id="checkbox_aceptar" value="si"> Acepto la <a href="<?php the_permalink(268); ?>" target="_blank">política de privacidad</a> del sitio.
                </label>
				</p>
				<div id="resultadoMensaje"></div>
				
				
				<p>
                    <input type="hidden" id="formulario" name="formulario" value="info" />
                    <input type="hidden" id="url" name="url" value="" />
                    <input type="submit" name="enviar" id="enviar_formulario" value="Enviar" class="enviar" />
				</p>

        </form>
		
	</div>
            	<div class="contactInfo">
                	<?php the_content();?>
                </div>
</div>

	
        <?php ETG_contenido(get_the_ID()); ?>
        <?php #require_once('includes/descarga-dossier.php'); ?>
        <?php #require_once('includes/formulario.php'); ?>
	</section>



<?php endwhile; ?>
<?php get_footer(); ?>




