<?php
    $GLOBALS['classes_to_body'] = 'blog';
    get_header();
?>
    <section class="container" style="padding-top: 0;">
        <?php
        $categorias = get_categories(array('hide_empty' => 0));
        ?>
            
            <div class="pagePanel clear">
            	<ul class="productFilter  clear">
                	
                	
                	<li id="parent-<?php the_ID(); ?>" class="parent-page primer">
                	    <strong>CATEGORIAS:</strong>
                	</li>
                	
                <?php foreach ($categorias as $categoria){ ?>
            
                    <li id="parent-<?php the_ID(); ?>" class="parent-page">
                        <a href="<?php echo get_category_link($categoria->term_id); ?>"><?php echo $categoria->name; ?></a>
                        
                    </li>
                <?php } ?>
            	</ul>
            </div>	
        
        <?php 
        if ( have_posts() ) { 
            ?>
            <div class="blogPostWrap">
                <?php
                $i = 0;
                while ( have_posts() ) { 
                    the_post();
                        ?>
                            
			<div class="postItem">
				<?php if(has_post_thumbnail()){ ?>
                <a href="<?php the_permalink(); ?>" class="postItemImg">
					<?php the_post_thumbnail('poster'); ?>
				</a>
				<?php } ?>
				<time class="postItemTime" datetime="2015-02-01"><?php the_date(); ?></time>
				<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
				<p><?php the_excerpt(); ?></p>
			</div>
                            
                        <?php
                    

                        $i++;
                }
                ?>
            </div>
            
            
            <div class="container pd50_0">
            	<div class="row">
            	    <div class="col-md-12 text-center">
                        <?php posts_nav_link( ' &#183; ', '&laquo; '.__('Aurreko artikuluak', 'ETG_text_domain'), __('Hurrengo artikuluak', 'ETG_text_domain').' &raquo;' ); ?>
            	    </div><!-- .col-md-12 -->
            	</div><!-- .row -->
            </div><!-- .container -->
        <?php } wp_reset_postdata(); ?>
	</article>
	
<?php #get_sidebar(); ?>
<?php get_footer(); ?>