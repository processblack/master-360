<?php 
    // Template Name: Home
    get_header();
?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>


	<section class="container">
        <?php if( have_rows('slides') ): ?>
    		<div class="homeBxSliderWrap">
    			<div class="homeBxSlider">
    				<?php 
        				$i = 0;
        				while( have_rows('slides') ): the_row(); 
                        $image = get_sub_field('fotografia');
                    ?>
    				<div class="slide <?php if ($i == 0) { echo 'active'; } ?>" data-slide="<?php echo $i; ?>" style="background-image: url(<?php echo $image['sizes']['slide']; ?>);">
    					<div class="slideDesc">
    						<h2><?php echo get_sub_field('texto'); ?></h2>
    						<a class="learnMore" href="<?php echo get_permalink(get_sub_field('enlace')); ?>"><?php echo get_sub_field('texto_boton'); ?></a>
    					</div>
    				</div>
    				<?php 
        				$i++; 
        				endwhile;
                    ?>
    			</div>
    		</div>	
        <?php endif; ?>

<!--

		<div class="homeBxSliderWrap">
			<div class="homeBxSlider">
				<div class="slide active" data-slide="0" style="background-image: url(images/home/slide-01.jpg);">
					<div class="slideDesc">
						<h2>Master Self Coaching 360º</h2>
						<a class="learnMore" href="<?php bloginfo( 'template_url' ); ?>/master-360/">Saber más</a>
					</div>
				</div>
				<div class="slide" data-slide="1" style="background-image: url(images/home/slide-02.jpg);">
					<div class="slideDesc">
						<h2>10.000 Sonrisas</h2>
						<a class="learnMore" href="<?php bloginfo( 'template_url' ); ?>/master-360/">Saber más</a>
					</div>
				</div>
				<div class="slide" data-slide="2" style="background-image: url(images/home/slide-03.jpg);">
					<div class="slideDesc">
						<h2>Empresas 360</h2>
						<a class="learnMore" href="<?php bloginfo( 'template_url' ); ?>/master-360/">Saber más</a>
					</div>
				</div>
			</div>
		</div>	
-->

		
        <?php ETG_contenido(get_the_ID()); ?>
		
	</section>





<?php endwhile; ?>
<?php get_footer(); ?>
