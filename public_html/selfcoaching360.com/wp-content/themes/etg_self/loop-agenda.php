    <?php 
    if (count($resultados) > 0){
        $i = 0;
        foreach ($resultados as $key=>$value){
            
            $fecha = explode('-', $value['fecha']);
            if (!isset($mes)){
                    $mes = $fecha[1];
                    ?>
                        <div style="padding: 20px 0"><h2 style="text-transform: uppercase; font-size: 30px;"><?php echo ucfirst(strftime("%B", strtotime($value['fecha']))); ?></h2></div>
                    <?php
            } else {
                if ($fecha[1] !== $mes) {
                    $mes = $fecha[1];
                    ?>
                        <div style="padding: 20px 0"><h2 style="text-transform: uppercase; font-size: 30px;"><?php echo ucfirst(strftime("%B", strtotime($value['fecha']))); ?></h2></div>
                    <?php
                }            
            }            
    ?>

            <div class="eventItem event-lecture clear">
<!--
            	<a href="<?php the_permalink(); ?>" class="eventItemImg">
            		<?php 
            			if (get_the_post_thumbnail($value['idEvento'])){
            				echo get_the_post_thumbnail($value['idEvento'], 'postergrande');
            			} else {
            				switch(ETG_categorias ($value['idEvento'])){
            					case 'Taller de las tres decisiones';
            					    $imagen = 'http://www.selfcoaching360.com/wp-content/themes/etg_self/images/eventos/taller.jpg';        					
            					break;
            					case 'Jornadas de puertas abiertas';
            					    $imagen = 'http://www.selfcoaching360.com/wp-content/themes/etg_self/images/eventos/puertas-abiertas.jpg';        					
            					break;
            					case 'Conferencias';
            					    $imagen = 'http://www.selfcoaching360.com/wp-content/themes/etg_self/images/eventos/conferencia.jpg';        					
            					break;
            					case '10.000 sonrisas';
            					    $imagen = 'http://www.selfcoaching360.com/wp-content/themes/etg_self/images/eventos/default.jpg';        					
            					break;
            					default:
            					    $imagen = 'http://www.selfcoaching360.com/wp-content/themes/etg_self/images/eventos/default.jpg';
            					break;
            				}
            				?>
            				<img src="<?php echo $imagen; ?>" alt="<?php echo get_the_title($value['idEvento']); ?>" />
            				<?php
            			}
            		?>
            		
            	</a>
-->
            	<div class="eventItemDesc">
            		<time class="eventItemTime <?php echo ETG_categorias_slug ($value['idEvento']); ?>"><?php echo ETG_categorias ($value['idEvento']); ?></time>
            		<h3><a href="<?php the_permalink($value['idEvento']); ?>"><?php echo get_the_title($value['idEvento']); ?></a></h3>
            		<p>
            		    <strong>Fecha</strong>: <?php echo get_field('fecha', $value['idEvento']); ?><br />
            		    <?php if (get_field('horario', $value['idEvento'])) { ?>
            		    <strong>Horario:</strong> <?php echo get_field('horario', $value['idEvento']); ?><br />
            		    <?php } ?>
            		    <?php if (get_field('precio', $value['idEvento'])) { ?>
            		    <strong>Precio:</strong>  <?php echo get_field('precio', $value['idEvento']); ?><br />
            		    <?php } ?>
            		    <?php if (get_field('lugar', $value['idEvento'])) { ?>
            		    <strong>Lugar:</strong>   <?php echo get_field('lugar', $value['idEvento']); ?>	
            		    <?php } ?>
            	    </p>
            	    <?php if (get_field('link_reserva', $value['idEvento'])) { ?>
            		<a href="<?php echo get_field('link_reserva', $value['idEvento']); ?>" class="eventLearnMore" target="_blank">Reservar</a>
            	    <?php } ?>
            	</div>
            </div>
        <?php }
            }
        ?>