<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);



// include custom jQuery
function shapeSpace_include_custom_jquery() {

	wp_deregister_script('jquery');
	wp_enqueue_script('jquery', get_bloginfo( 'template_url' ).'/js/jquery-1.9.1.min.js', array(), null, true);
	wp_enqueue_script('selectric', get_bloginfo( 'template_url' ).'/js/jquery.selectric.min.js', array(), null, true);
	wp_enqueue_script('bxslider', get_bloginfo( 'template_url' ).'/js/jquery.bxslider.min.js', array(), null, true);
	wp_enqueue_script('script', get_bloginfo( 'template_url' ).'/js/script.js', array(), null, true);

}
add_action('wp_enqueue_scripts', 'shapeSpace_include_custom_jquery');

    
function set_idiomas() {
    global $idioma, $locale,$fecha;
    
    $fecha = date('Y-m-d');
    if (defined('ICL_LANGUAGE_CODE')){
        $idioma = ICL_LANGUAGE_CODE;
    } else {
        define('ICL_LANGUAGE_CODE', 'es');
    }
	switch ($idioma){
		case "es":
            define('MY_LOCALE', 'es_ES.UTF-8');
            define('IDIOMA_AGENDA', 'es');
            define('IDIOMA_NOTICIAS', 'es');
		break;
		case "eu":
            define('MY_LOCALE', 'eu_ES.UTF-8');
            define('IDIOMA_AGENDA', 'eu');
            define('IDIOMA_NOTICIAS', 'eu');
		break;
		case "en":
            define('MY_LOCALE', 'en_EN.UTF-8');
            define('IDIOMA_AGENDA', 'en');
            define('IDIOMA_NOTICIAS', 'es');
		break;
		case "fr":
            define('MY_LOCALE', 'fr_FR.UTF-8');
            define('IDIOMA_AGENDA', 'fr');
            define('IDIOMA_NOTICIAS', 'fr');
		break;
		default:
            define('MY_LOCALE', 'es_ES.UTF-8');
            define('IDIOMA_AGENDA', 'es');
            define('IDIOMA_NOTICIAS', 'es');
		break;
	}
	setlocale(LC_TIME, MY_LOCALE);
    $idioma = ICL_LANGUAGE_CODE;
}
add_action( 'init', 'set_idiomas' ); 

add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size( 360, 360, true );
#add_image_size( 'miniatura', 360, 360, true );
add_image_size( 'poster', 408, 272, true );
add_image_size( 'postergrande', 1004, 684, true );
add_image_size( 'cabecera', 1366, 560, true );
add_image_size( 'slide', 2000, 1336, true );

function custom_excerpt_length( $length ) {
	return 20;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


function registrar_sidebar(){  
  register_sidebar(array(  
   'name' => 'Sidebar del blog',  
   'id' => 'sidebar-blog',  
   'description' => 'Descripción de ejemplo',  
   'class' => 'sidebar',  
   'before_widget' => '<aside id="%1$s" class="widget %2$s">',  
   'after_widget' => '</aside>',  
   'before_title' => '<h2 class="widget-title">',  
   'after_title' => '</h2>',  
  ));  
  register_sidebar(array(  
   'name' => 'Sidebar de agenda',  
   'id' => 'sidebar-agenda',  
   'description' => 'Descripción de ejemplo',  
   'class' => 'sidebar',  
   'before_widget' => '<aside id="%1$s" class="widget %2$s">',  
   'after_widget' => '</aside>',  
   'before_title' => '<h2 class="widget-title">',  
   'after_title' => '</h2>',  
  ));  
}  
add_action( 'widgets_init', 'registrar_sidebar');  


function new_excerpt_more($more) {
    return '';
}
add_filter('excerpt_more', 'new_excerpt_more', 21 );

function the_excerpt_more_link( $excerpt ){
    $post = get_post();
    $excerpt .= '<a href="'. get_permalink($post->ID) . '" class="learnMore_mini">Leer entrada</a>';
    return $excerpt;
}
add_filter( 'the_excerpt', 'the_excerpt_more_link', 21 );


add_filter( 'embed_oembed_html', 'custom_oembed_filter', 10, 4 ) ;
function custom_oembed_filter($html, $url, $attr, $post_ID) {
    $return = '<div class="embed-container">'.$html.'</div>';
    return $return;
}

function ETG_categorias ($id){
    $li = '';
    if (get_post_type($id) == "agenda"){
        $tipo = get_the_terms($id, 'tipo');
        if(get_the_terms($id, 'tipo')){
            foreach($tipo as $c) {
            	$cat = get_category($c);
            	#print_r($cat);
                $li.=$cat->cat_name;
            }
        }
    } else {
        $cont = wp_get_post_categories($id);
        foreach($cont as $c) {
        	$cat = get_category($c);
        	#print_r($cat);
        	$li.=$cat->cat_name;
        }
    
    }
    
    return $li;
}
function ETG_categorias_slug ($id){
    $li = '';
    if (get_post_type($id) == "agenda"){
        $tipo = get_the_terms($id, 'tipo');
        if(get_the_terms($id, 'tipo')){
            foreach($tipo as $c) {
            	$cat = get_category($c);
            	#print_r($cat);
                $li.=$cat->category_nicename;
            }
        }
    } else {
        $cont = wp_get_post_categories($id);
        foreach($cont as $c) {
        	$cat = get_category($c);
        	#print_r($cat);
        	$li.=$cat->category_nicename;
        }
    
    }
    
    return $li;
}





    
# Soporte idiomas     
load_theme_textdomain('ETG_text_domain', get_template_directory() . '/languages');



// ******************* Sidebars ****************** //

if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
		'name' => 'Pages',
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h2>',
		'after_title' => '</h2>',
	));
}

// ******************* Add Custom Menus ****************** //

add_theme_support( 'menus' );


// ******************* Post Thumbnails ****************** //


/*
    CUSTOM POST TYPE    
*/
$labels = array(
	'name'               => _x( 'Proyectos', 'post type general name', 'ETG_text_domain' ),
	'singular_name'      => _x( 'Proyecto', 'post type singular name', 'ETG_text_domain' ),
	'menu_name'          => _x( 'Proyectos', 'admin menu', 'ETG_text_domain' ),
	'name_admin_bar'     => _x( 'Proyecto', 'add new on admin bar', 'ETG_text_domain' ),
	'add_new'            => _x( 'Agregar nuevo', 'book', 'ETG_text_domain' ),
	'add_new_item'       => __( 'Agregar nuevo proyecto', 'ETG_text_domain' ),
	'new_item'           => __( 'Nuevo proyecto', 'ETG_text_domain' ),
	'edit_item'          => __( 'Editar proyecto', 'ETG_text_domain' ),
	'view_item'          => __( 'Ver proyecto', 'ETG_text_domain' ),
	'all_items'          => __( 'Todos los proyectos', 'ETG_text_domain' ),
	'search_items'       => __( 'Buscar proyectos', 'ETG_text_domain' ),
	'parent_item_colon'  => __( 'Proyectos hijo:', 'ETG_text_domain' ),
	'not_found'          => __( 'No se encontraron proyectos.', 'ETG_text_domain' ),
	'not_found_in_trash' => __( 'No se encontraron proyectos en la papelera.', 'ETG_text_domain' )
);



register_post_type('proyectos', array(
	'labels' => $labels,
	'public' => true,
	'menu_icon' => 'dashicons-portfolio',
	'show_ui' => true,
	'capability_type' => 'post',
	'hierarchical' => true,
	'rewrite' => array( 'slug' => 'proyectos'),
	'query_var' => false,
	'has_archive' => true,
	'supports' => array('title', 'editor', 'author', 'thumbnail', 'revisions')
));


add_action( 'init', 'build_taxonomies', 0 );

function build_taxonomies() {
    register_taxonomy( 'tipo', 'proyectos', array( 'hierarchical' => true, 'label' => 'Tipo', 'show_in_nav_menus' => true, 'query_var' => true, 'rewrite' => array( 'slug' => 'tipo' ) ) ); 
}


/*
* Añade estilos personificados a la barra de herramientas de tiny_mce
*/
    // Callback function to insert 'styleselect' into the $buttons array
    function my_mce_buttons_2( $buttons ) {
    	array_unshift( $buttons, 'styleselect' );
    	return $buttons;
    }
    // Register our callback to the appropriate filter
    add_filter( 'mce_buttons_2', 'my_mce_buttons_2' );
    

    // Callback function to filter the MCE settings
    function my_mce_before_init_insert_formats( $init_array ) {  
    	// Define the style_formats array
    	$style_formats = array(  
    		// Each array child is a format with it's own settings 
/*
    		array(  
    			'title' => '.entradilla',  
    			'block' => 'p',  
    			'classes' => 'entradilla',
    			'wrapper' => false,
    		),
*/
    		array(  
    			'title' => '.boton',  
    			'selector' => 'a',  
    			'classes' => 'boton',
    			'wrapper' => false,
    		),
    		array(  
    			'title' => '.descarga',  
    			'selector' => 'a',  
    			'classes' => 'boton',
    			'wrapper' => false,
    		),
    		array(  
    			'title' => 'mayusculas',  
    			'selector' => 'p',  
    			'classes' => 'text-uppercase',
    			'wrapper' => false,
    		)
    	);  
    	// Insert the array, JSON ENCODED, into 'style_formats'
    	$init_array['style_formats'] = json_encode( $style_formats );  
    	
    	return $init_array;  
      
    } 
    // Attach callback to 'tiny_mce_before_init' 
    add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );  
    

function wpdocs_theme_add_editor_styles() {
    add_editor_style( 'css/custom-editor-style.css' );
}
add_action( 'admin_init', 'wpdocs_theme_add_editor_styles' );


function ETG_content_banda_de_texto ($id) {
    $contenido = get_sub_field('titular', $id);
    return $contenido;
}


function ETG_content_galeria_de_videos ($id) {
    $contenido = get_sub_field('titular', $id);
    return $contenido;
}


if ( function_exists( 'acf_add_options_sub_page' ) ){
	acf_add_options_sub_page(array(
		'title'      => 'Opciones página agenda',
		'parent'     => 'edit.php?post_type=agenda',
		'capability' => 'manage_options'
	));
}


function borrar_datos_fechas($post_id) {
    $args = array(
            'post_type' => 'agenda',
            'post_ID' => $post_id
    );
    $the_query = new WP_Query( $args );
    if ( $the_query->have_posts() ) {
    	while ( $the_query->have_posts() ) {
    		$the_query->the_post();
    		if( have_rows('fechas') ){
                if (mysql_connect(DB_HOST, DB_USER, DB_PASSWORD)){
                    mysql_query("DELETE FROM wp_fechas WHERE idEvento = ".$post_id);
                } else {
                    echo "Error al conectar a la base de datos.";
                }
    		}
    	}
    }
}


function guardar_datos_fechas($post_id) {
    /* 
        Esta función se ejecuta cada vez que un evento
        es guardado en el administardor del eventos de WP
    */
    $args = array(
            'post_type' => 'agenda',
            'p' => $post_id
    );
    #die(print_r($args));
    $the_query = new WP_Query( $args );
    if ( $the_query->have_posts() ) {
    	while ( $the_query->have_posts() ) {
    		$the_query->the_post();
    		if (get_post_status($post_id) == 'publish'){
                if (mysql_connect(DB_HOST, DB_USER, DB_PASSWORD)){
                    mysql_select_db(DB_NAME);
                    mysql_query("DELETE FROM wp_fechas WHERE idEvento = ".$post_id);
                		$fecha = explode('/',get_field('fecha'));
                		$sql = "INSERT INTO wp_fechas (idEvento, fecha, idioma) VALUES ('".$post_id."', '".$fecha[2]."-".$fecha[1]."-".$fecha[0]."', '".IDIOMA_AGENDA."')";
                		$resultado = mysql_query($sql);
                		if (!$resultado) {
                			die("Error-> ".mysql_error());
                		}
                } else {
                    echo "Error al conectar a la base de datos.";
                }
            }
    	}
    }
}

add_action( 'save_post', 'guardar_datos_fechas' );
add_action( 'before_delete_post', 'borrar_datos_fechas' );


function ETG_contenido($idPost) {
    if( have_rows('crear_contenido', $idPost) ):
        while ( have_rows('crear_contenido', $idPost) ) : the_row();
            if( get_row_layout() == 'banda_de_foto' ):
                get_template_part('contenidos/banda-de-foto');
            
            elseif( get_row_layout() == 'banda_de_texto' ): 
                get_template_part('contenidos/banda-de-texto');
            
            elseif( get_row_layout() == 'paginas_hijo' ): 
                get_template_part('contenidos/paginas-hijo');
            
            elseif( get_row_layout() == 'eventos' ): 
                get_template_part('contenidos/eventos');
            
            elseif( get_row_layout() == 'galeria_de_fotos' ): 
                get_template_part('contenidos/galeria-de-fotos');
            
            elseif( get_row_layout() == 'galeria_de_videos' ): 
                get_template_part('contenidos/galeria-de-videos');
            
            elseif( get_row_layout() == 'descarga_dossier_selfcoaching' ): 
                get_template_part('contenidos/descarga-dossier');
            
            elseif( get_row_layout() == 'descarga_dossier_pes' ): 
                get_template_part('contenidos/descarga-dossier-pes');
            
            elseif( get_row_layout() == 'banda_de_video' ): 
                get_template_part('contenidos/banda-de-video');
            
            elseif( get_row_layout() == 'redes_sociales' ): 
                get_template_part('contenidos/redes_sociales');
                
            elseif( get_row_layout() == 'confian_en_nosotros' ): 
                get_template_part('contenidos/confian_en_nosotros');
            
            elseif( get_row_layout() == 'en_los_medios' ): 
                get_template_part('contenidos/en_los_medios');
            
            elseif( get_row_layout() == 'banda_de_destacados' ): 
                get_template_part('contenidos/banda-de-destacados');
            
            elseif( get_row_layout() == 'texto_con_titular_de_color' ): 
                get_template_part('contenidos/texto-con-titular-de-color');
            
            elseif( get_row_layout() == 'testimonios' ): 
                get_template_part('contenidos/testimonios');
            
            elseif( get_row_layout() == 'ultimas_noticias' ): 
                get_template_part('contenidos/ultimas-noticias');
            
            elseif( get_row_layout() == 'formulario_te_interesa_el_master' ): 
                get_template_part('contenidos/formulario');
            
            endif;
        endwhile;
    endif;
    
}



function ETG_activar_custom_post_type() {
        
    $labels = array(
    	'name'               => _x( 'Agenda', 'post type general name', 'ETG_kursaal' ),
    	'singular_name'      => _x( 'Evento', 'post type singular name', 'ETG_kursaal' ),
    	'menu_name'          => _x( 'Eventos', 'admin menu', 'ETG_kursaal' ),
    	'name_admin_bar'     => _x( 'Evento', 'add new on admin bar', 'ETG_kursaal' ),
    	'add_new'            => _x( 'Agregar nuevo', 'book', 'ETG_kursaal' ),
    	'add_new_item'       => __( 'Agregar nuevo evento', 'ETG_kursaal' ),
    	'new_item'           => __( 'Nuevo evento', 'ETG_kursaal' ),
    	'edit_item'          => __( 'Editar evento', 'ETG_kursaal' ),
    	'view_item'          => __( 'Ver evento', 'ETG_kursaal' ),
    	'all_items'          => __( 'Todos los eventos', 'ETG_kursaal' ),
    	'search_items'       => __( 'Buscar eventos', 'ETG_kursaal' ),
    	'parent_item_colon'  => __( 'Eventos hijo:', 'ETG_kursaal' ),
    	'not_found'          => __( 'No se encontraron eventos.', 'ETG_kursaal' ),
    	'not_found_in_trash' => __( 'No se encontraron eventos en la papelera.', 'ETG_kursaal' )
    );
    
    
    
    register_post_type('agenda', array(
    	'labels' => $labels,
    	'public' => true,
    	'menu_icon' => 'dashicons-calendar-alt',
    	'show_ui' => true,
    	'capability_type' => 'post',
    	'hierarchical' => false,
    	'rewrite' => array( 'slug' => 'agenda','with_front' => true),
    	'query_var' => true,
    	'has_archive' => true,
    	'supports' => array('title', 'editor', 'author', 'thumbnail', 'revisions')
    ));
    
    register_taxonomy( 'tipo', 'agenda', array( 'hierarchical' => true, 'label' => 'Tipo', 'show_in_nav_menus' => true, 'query_var' => true, 'rewrite' => array( 'hierarchical' => true ) ) ); 
}

add_action( 'init', 'ETG_activar_custom_post_type', 0 );


