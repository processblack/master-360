<div class="etg_modulo galeria">
    <div class="container">
    	<div class="row">
    	    <div class="col-md-12">
                <h3 class="text-uppercase"><?php echo get_sub_field('titular', $id); ?></h3>
    	    </div><!-- .col-md-12 -->
    	</div><!-- .row -->
    	<div class="row">
        	<div class="galeria-masonry">
            <?php
            $images = get_sub_field('galeria', $id);
            
            if( $images ){
                foreach( $images as $image ){
                ?>
                <div class="item">
                    <a href="<?php echo $image['url']; ?>" class="boxer" rel="roadtrip">
                        <img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt'] ?>" class="img-responsive" />
                    </a>        	
                </div><!-- .col-md-3 -->
                <?php }
            }
            ?>	    
            </div>
    	</div><!-- .row -->
    </div><!-- .container -->
</div>



