<div class="etg_modulo paginas_hijo">
		<?php if (get_sub_field('titular') or get_sub_field('texto')) {  ?>
		<div class="wrapper">
		<div class="singlePostWrap">
			<?php if (get_sub_field('titular')){ echo '<h2 class="center uppercase">'.get_sub_field('titular').'</h2>'; }  ?>
			<?php if (get_sub_field('texto')){ echo get_sub_field('texto'); } ?>
        </div>
        </div>
        <?php } ?>
		<div class="blogPostWrap">
            <?php
            $paginas = get_pages( array( 'child_of' => $post->ID, 'parent' => $post->ID, 'sort_order' => 'ASC', 'sort_column' => 'menu_order'));
            
            foreach( $paginas as $pagina ) {
            ?>
			<div class="postItem">
				<a href="<?php echo get_page_link( $pagina->ID ); ?>" class="postItemImg">
					<?php 
    				if (has_post_thumbnail($pagina->ID)) {
					    echo get_the_post_thumbnail($pagina->ID, 'poster');
					} ?>
				</a>
				<h4><a href="<?php echo get_page_link( $pagina->ID ); ?>"><?php echo $pagina->post_title; ?></a></h4>
				<?php if (get_field('subtitulo', $pagina->ID)){ ?>
				<p><?php echo get_field('subtitulo', $pagina->ID); ?></p>
				<?php } ?>
			</div>
            <?php
            }	
            ?>
        </div>        
</div>