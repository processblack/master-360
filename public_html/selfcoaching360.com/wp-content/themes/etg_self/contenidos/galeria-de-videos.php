<?php
    if( have_rows('videos') ) {
    while ( have_rows('videos') ) : the_row();
        ?>
        <div class="etg_modulo galeria-de-video" style="background: black; padding: 50px 0;">
            <div class="container">            	
                <div class="row">
                    <div class="col-md-4">
                        <h3 class="text-uppercase blanco"><?php the_sub_field('titulo'); ?></h3>
                        <div class="texto-blanco">
                            <?php the_sub_field('texto'); ?>
                        </div>
                    </div><!-- .col-md-4 -->
                    <div class="col-md-8">
                        <div class="embed-container">
                            <?php the_sub_field('video'); ?>
                        </div>
                    </div><!-- .col-md-12 -->
                </div><!-- .row -->
            </div><!-- .container -->
        </div>
        
        <?php
    endwhile;
    }
?>	    
