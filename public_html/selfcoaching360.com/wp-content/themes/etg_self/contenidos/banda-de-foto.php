<?php 
    $foto        = get_sub_field('fotografia');
    $color_texto = get_sub_field('color_texto');
    if ($color_texto == ''){
        $color_texto = '#fff';
    }
    
    if (get_sub_field('alinea_imagen_destacada')){
        $alinear_imagen_destacada = get_sub_field('alinea_imagen_destacada');
    } else {
        $alinear_imagen_destacada = '50%';
    }
    
?>



<div class="etg_modulo banda-de-foto">
    <div class="pageHeader" style="background: <?php echo get_sub_field('color'); ?> url(<?php echo $foto['url']; ?>) no-repeat <?php echo $alinear_imagen_destacada; ?>; background-size: cover;">
        <h1 class=""><?php echo get_sub_field('texto'); ?></h1>
    </div>
</div>