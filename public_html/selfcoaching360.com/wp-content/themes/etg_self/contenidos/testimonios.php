<div class="etg_modulo testimonios blogPosts">
	<?php if (get_sub_field('titular')){ ?>
	<div class="blockTitle"><h2><?php the_sub_field('titular'); ?></h2></div>
	<?php } ?>
	<div class="blogPostWrap">
    <?php 
    if (get_sub_field('tipo') == 'video') {
        $campo = 'testimonios_video';
    } else {
        $campo = 'testimonios';
    }

    
    if( have_rows($campo) ) {
    while ( have_rows($campo) ) : the_row();
        $foto = get_sub_field('imagen');
        ?>
		<div class="<?php echo ($campo == 'testimonios_video') ? 'postItem video' : 'postItem'; ?>">
			<?php if ($campo == 'testimonios_video') {?>
			    <div class="embed-container"><?php the_sub_field('texto'); ?></div>
			<?php } else {
    			if (get_sub_field('self')) {
        			?>
        			 <blockquote class="icono"><p><?php the_sub_field('texto'); ?></p></blockquote>
        			<?php
    			} else {
        			?>
        			 <blockquote><p><?php the_sub_field('texto'); ?></p></blockquote>
        			<?php
    			}
			?>
			   
			<?php }?>
            <?php if (get_sub_field('autor') and $campo !== 'testimonios_video') { ?>
            <p class="author"><img src="<?php echo $foto['sizes']['thumbnail']; ?>" alt="autor" style="width: 50px; height: auto;">
			<?php the_sub_field('autor'); ?></p>
			<?php } ?>
		</div>
        <?php
    endwhile;
    }
    ?>
	</div>
</div>