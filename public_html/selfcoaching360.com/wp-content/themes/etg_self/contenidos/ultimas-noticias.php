
		<div id="ultimas-noticias" class="etg_modulo ">
    		
    		<div class="blogPosts">
    		
			<h2 class="blockTitle"><?php if (get_sub_field('titular')){ echo get_sub_field('titular'); } else { echo 'Últimas noticias'; } ?></h2>
			<?php if (get_sub_field('texto')){ echo get_sub_field('texto'); } ?>
            </div>

		</div>



        <?php 
        $the_query = new WP_Query( array('posts_per_page' => 3) );    
            
            
        if ( $the_query->have_posts() ) :
            ?>
            <div class="blogPostWrap">
                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                            
			<div class="postItem">
				<?php if(has_post_thumbnail()){ ?>
                <a href="<?php the_permalink(); ?>" class="postItemImg">
					<?php the_post_thumbnail('poster'); ?>
				</a>
				<?php } ?>
				<time class="postItemTime" datetime="2015-02-01"><?php the_date(); ?></time>
				<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
				<p><?php the_excerpt(); ?></p>
			</div>
			
			<?php endwhile; ?>
	<!-- end of the loop -->

	<!-- pagination here -->

	<?php wp_reset_postdata(); ?>
			
            </div>
                            
                        <?php
                    

        endif;
                ?>
            
            
