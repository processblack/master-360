<div class="etg_modulo blogPosts"<?php if (get_sub_field('color_de_fondo')) { echo 'style="background-color: '.get_sub_field('color_de_fondo').'"'; } ?>>
    
	<?php if (get_sub_field('titular')){ ?>
	<div class="blockTitle"><h2><?php the_sub_field('titular'); ?></h2></div>
	<?php } ?>
    
	<div class="blogPostWrap">
    <?php if( have_rows('destacado') ) {
    while ( have_rows('destacado') ) : the_row();
        $foto = get_sub_field('foto');
        ?>
		<div class="postItem <?php if (!get_sub_field('foto')) { echo 'sinfoto'; } ?>">
			<a href="<?php the_sub_field('link'); ?>" class="postItemImg">
				<img src="<?php echo $foto['sizes']['poster']; ?>" alt="">
			</a>
			<h4>
    			<?php if (get_sub_field('link')) { ?>
    			<a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('titulo'); ?></a>
    			<?php } else { ?>
    			<?php the_sub_field('titulo'); ?>
    			<?php } ?>
            </h4>
			<p><?php the_sub_field('texto'); ?></p>
		</div>
        <?php
    endwhile;
    }
    ?>
	</div>
</div>

<div class="blogPostWrap">

<?php
    $tipo = get_sub_field('tipo_de_evento');
    $numero_de_eventos = 0;


	$la_fecha = date('Y-m-d');
    $sqlAgenda = "SELECT * FROM (select * from wp_fechas where (fecha >= '".$la_fecha."') and (idioma = '".IDIOMA_AGENDA."') order by fecha asc) as temp_table group by idEvento order by fecha asc;";
    echo '<!--'.$sqlAgenda.'-->';
    #FIND_IN_SET
	global $wpdb;
	$resultados = $wpdb->get_results( $sqlAgenda, ARRAY_A );    		
	set_query_var('resultados', $resultados); # pasa los valores
        
    foreach ($resultados as $key=>$value){
        #echo $value['idEvento'].'<br />';
        if(!has_term( $tipo, 'tipo', $value['idEvento'])) {
            unset($resultados[$key]);
        }
    }
    
    foreach ($resultados as $resultado){
	    $idEvento = $resultado['idEvento']; ?>
		<div class="postItem sinfoto">
			<h4>
    			<a href="<?php the_permalink($idEvento); ?>"><?php echo get_the_title($idEvento); ?></a>
            </h4>
            		<p>
					    <strong>Fecha:</strong>   <?php echo get_field('fecha', $idEvento); ?><br />
					    <strong>Horario:</strong> <?php echo get_field('horario', $idEvento); ?><br />
					    <strong>Precio:</strong>  <?php echo get_field('precio', $idEvento); ?><br />
					    <strong>Lugar:</strong>   <?php echo get_field('lugar', $idEvento); ?>	
				    </p>
				    
				    <?php if (get_field('link_reserva', $idEvento)) { ?>
					<p style="padding: 50px 0;"><a href="<?php echo get_field('link_reserva', $idEvento); ?>" class="boton" target="_blank">Reservar</a></p>
				    <?php } ?>
				    
		</div>
    <?php }

    
/*
    $args = array(
    	'post_type' => 'eventos',
    	'tax_query' => array(
    		'relation' => 'AND',
    		array(
    			'taxonomy' => 'movie_genre',
    			'field'    => 'slug',
    			'terms'    => array( 'action', 'comedy' ),
    		),
    		array(
    			'taxonomy' => 'actor',
    			'field'    => 'term_id',
    			'terms'    => array( 103, 115, 206 ),
    			'operator' => 'NOT IN',
    		),
    	),
    );
    $query = new WP_Query( $args );    
*/
    
    
?>

</div>