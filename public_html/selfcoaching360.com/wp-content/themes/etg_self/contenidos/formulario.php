<div id="formulario" class="etg_modulo classesBox-formulario">
    <div class="wrapper">
        <div class="singlePostWrap">	
			<h2 class="center uppercase"><?php if (get_sub_field('titular')){ echo get_sub_field('titular'); } else { echo '¿TE INTERESA EL MÁSTER?'; } ?></h2>
			<?php if (get_sub_field('texto')){ echo get_sub_field('texto'); } else { echo '<p class="center">Solicita tu entrevista personal. En breve nos pondremos en contacto contigo.</p>'; } ?>
			<form id="form1" name="form1" method="post" action="" class="center-block">    			
				<div class="izquierda">
    				<p class="">
    					<label for="nombre">Nombre</label>
    					<input type="text" name="nombre" id="nombre" />
    				</p>
    				<p class="">
    					<label for="localidad">Localidad</label>
    					<input type="text" name="localidad" id="localidad" />
    				</p>
				</div>
				<div class="derecha">
    				<p class="">
                        <label for="telefono">Teléfono</label>
    					<input type="text" name="telefono" id="telefono" />
    				</p>
    				
    				<p class="">
                        <label for="email">E-mail</label>
    					<input type="text" name="email" id="email" />
    				</p>
				</div>
				<?php 
    				if (get_sub_field('campo_de_interes')) {
        				echo '<input type="hidden" id="campo_de_interes" name="campo_de_interes" value="'.get_sub_field('campo_de_interes').'" />';
    				} else {
    					$campos = get_sub_field_object('campo_de_interes'); 
                        echo '<p><label>Campo de interés</label></p>';
                        #echo '<label for="formulario">Campo de interés</label>';
                        #echo '<select id"formulario" name="formulario">';
                        #foreach ($campos['choices'] as $campo) {
                        #    echo '<option value="'.$campo.'">'.$campo.'</option>';
                        #}
                        #echo '</select>';
                        foreach ($campos['choices'] as $campo) {
                            echo '<p class="izquierda"><input type="checkbox" id="campo_de_interes[]" name="campo_de_interes" value="'.$campo.'"> '. $campo.'</p>';
                        }
    				}
    				
                ?>
				
<!--
				<p style="clear: both">
                
                    <input type="checkbox" id="productos_1" value="si"> <label for="productos_1">Taller 3 decisiones</label><br />
                    <input type="checkbox" id="productos_2" value="si"> <label for="productos_2">Conferencias</label><br />
                    <input type="checkbox" id="productos_3" value="si"> <label for="productos_3">Jornadas de puertas abiertas</label><br />
                    <input type="checkbox" id="productos_4" value="si"> <label for="productos_4">Suscripción al blog</label><br />
                    <input type="checkbox" id="productos_5" value="si"> <label for="productos_5">Empresas 360º</label><br />

				</p>
-->


				<p style="clear: both">
                <label>
                    <input type="checkbox" id="checkbox_aceptar" value="si"> Acepto la <a href="<?php the_permalink(268); ?>" target="_blank" style="color: white;">política de privacidad</a> del sitio.
                </label>
				
				<?php if (is_user_logged_in()){ ?>
				<p style="clear: both">
                <label>
                    <input type="checkbox" id="checkbox_aceptar" value="si"> Deseo recibir información comercial de Selfcoaching 360º
                </label>
				<?php } ?>
				</p>
				
				
				<div id="resultadoMensaje"></div>
				
				
				<p>
                    <input type="hidden" id="url" name="url" value="" />
                    <?php $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>
                    <input type="hidden" id="id-dossier" name="id-dossier" value="<?php echo $post->ID; ?>" />
                    <input type="hidden" id="source" name="source" value="<?php echo $actual_link; ?>" />
                    <input type="hidden" id="etiqueta_asunto" name="etiqueta_asunto" value="<?php echo (get_sub_field('etiqueta_asunto')) ? get_sub_field('etiqueta_asunto') : '[GENERAL]'; ?>" />
                    <input type="hidden" id="enviar_a" name="enviar_a" value="<?php echo (get_sub_field('enviar_a')) ? str_replace('@', 'ARROBA', get_sub_field('enviar_a')) : 'infoARROBAselfcoaching360.com'; ?>" />
                    <input type="submit" name="enviar" id="enviar_formulario" value="Enviar" class="enviar" />
				</p>
				
				<?php if (is_user_logged_in()){ ?>
                    <div class="nota_datos">
                        <p><strong>INFORMACIÓN BÁSICA SOBRE PROTECCIÓN DE DATOS</strong></p>
                        <table>
                            <tr>
                                <th>Responsable</th>
                                <td>SELFCOACHING 360 S.L.U.</td>
                            </tr>
                            <tr>
                                <th>Finalidad</th>
                                <td>Gestionar las peticiones de información y el envío de newsletter</td>
                            </tr>
                            <tr>
                                <th>Legitimación</th>
                                <td>Consentimiento del interesado/a</td>
                            </tr>
                            <tr>
                                <th>Destinatarios</th>
                                <td>No hay ninguna cesión de datos</td>
                            </tr>
                            <tr>
                                <th>Derechos</th>
                                <td>Acceder, rectificar y suprimir los datos, así como otros derechos, como se explica en la información adicional.</td>
                            </tr>
                            <tr>
                                <th>Información adicional</th>
                                <td>Puede consultar la información adicional y detallada sobre Protección de Datos en nuestra <a href="/politica-de-cookies">Política de privacidad</a>, <a href="/aviso-legal">Aviso Legal</a> y <a href="/politica-de-cookies">Política de cookies</a>.</td>
                            </tr>
                        </table>
                        <br /><br />
                        <small>
                        <strong>INFORMACIÓN BÁSICA SOBRE PROTECCIÓN DE DATOS</strong><br />
                        <strong>Responsable</strong>: 
                        SELFCOACHING 360 S.L.U.,<br />
                        <strong>Finalidad</strong>: 
                        Gestionar las peticiones de información y el envío de newsletter<br />
                        <strong>Legitimación</strong>: 
                        Consentimiento del interesado/a <br />
                        <strong>Destinatarios</strong>: 
                        No hay ninguna cesión de datos,<br /> 
                        <strong>Derechos</strong>: 
                        Acceder, rectificar y suprimir los datos, así como otros derechos, como se explica en la información adicional. <br />
                        <strong>Información adicional</strong>: 
                        Puede consultar la información adicional y detallada sobre Protección de Datos en nuestra <a href="/politica-de-cookies">Política de privacidad</a>, <a href="/aviso-legal">Aviso Legal</a> y <a href="/politica-de-cookies">Política de cookies</a>.
                        </small>
				<?php } ?>
				
			</form>
		</div>
    </div>
</div>