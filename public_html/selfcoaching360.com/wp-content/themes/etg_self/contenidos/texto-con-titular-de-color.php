        <div class="etg_modulo classesBox" data-type="parallax" data-speed="10" style="background-color: <?php the_sub_field('color_de_fondo'); ?>; height: auto;">
            <a href="#" class="classesCategory"><?php the_sub_field('subtitulo'); ?></a>
            <h3><?php the_sub_field('titular'); ?></h3>
        </div>		
		
		<div class="wrapper">
			<div class="singlePostWrap">    
                
                <?php the_sub_field('texto'); ?>
                
            </div>
        </div>

