<div class="etg_modulo blogPosts"<?php if (get_sub_field('color_de_fondo')) { echo 'style="background-color: '.get_sub_field('color_de_fondo').'"'; } ?>>
    
	<?php if (get_sub_field('titular')){ ?>
	<div class="blockTitle"><h2><?php the_sub_field('titular'); ?></h2></div>
	<?php } ?>
    
	<div class="blogPostWrap">
    <?php if( have_rows('destacado') ) {
    while ( have_rows('destacado') ) : the_row();
        $foto = get_sub_field('foto');
        ?>
		<div class="postItem <?php if (!get_sub_field('foto')) { echo 'sinfoto'; } ?>">
			<a href="<?php the_sub_field('link'); ?>" class="postItemImg">
				<img src="<?php echo $foto['sizes']['poster']; ?>" alt="">
			</a>
			<h4>
    			<?php if (get_sub_field('link')) { ?>
    			<a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('titulo'); ?></a>
    			<?php } else { ?>
    			<?php the_sub_field('titulo'); ?>
    			<?php } ?>
            </h4>
			<p><?php the_sub_field('texto'); ?></p>
		</div>
        <?php
    endwhile;
    }
    ?>
	</div>
</div>