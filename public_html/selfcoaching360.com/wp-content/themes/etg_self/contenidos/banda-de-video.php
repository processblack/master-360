<div class="etg_modulo banda-de-video">
	<div class="wrapper">
        <div class="blockTitle"><h2 style="margin-bottom: 50px"><?php the_sub_field('titulo'); ?></h2></div>        	
        <div class="embed-container"><?php the_sub_field('video'); ?></div>
    </div>
</div>