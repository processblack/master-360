		<div id="redes-sociales" class="etg_modulo classesBox-descarga">
    		<div class="contenedor">
    			<h2 class="center uppercase">Síguenos</h2>
    			<p class="center" style="padding-bottom: 30px">Sigue las novedades de la escuela en tu red social favorita.</p>

                <p style="text-align: center;">
                    <a href="https://www.facebook.com/selfcoaching360/" target="_blank"><img src="<?php bloginfo( 'template_url' ); ?>/images/iconos/facebook.png" srcset="<?php bloginfo( 'template_url' ); ?>/images/iconos/facebook@2x.png 2x" alt="Facebook" /></a>
                    <a href="http://www.instagram.com/selfcoaching360" target="_blank"><img src="<?php bloginfo( 'template_url' ); ?>/images/iconos/instagram.png" srcset="<?php bloginfo( 'template_url' ); ?>/images/iconos/instagram@2x.png 2x" alt="instagram" /></a>
                    <a href="https://twitter.com/selfcoaching360" target="_blank"><img src="<?php bloginfo( 'template_url' ); ?>/images/iconos/twitter.png" srcset="<?php bloginfo( 'template_url' ); ?>/images/iconos/twitter@2x.png 2x" alt="twitter" /></a>
                    <a href="http://www.youtube.com/user/iflecuona" target="_blank"><img src="<?php bloginfo( 'template_url' ); ?>/images/iconos/youtube.png" srcset="<?php bloginfo( 'template_url' ); ?>/images/iconos/youtube@2x.png 2x" alt="youtube" /></a>
                    <a href="http://www.linkedin.com/company-beta/11062408" target="_blank"><img src="<?php bloginfo( 'template_url' ); ?>/images/iconos/linkedin.png" srcset="<?php bloginfo( 'template_url' ); ?>/images/iconos/linkedin@2x.png 2x" alt="linkedin" /></a>
                    <a href="https://www.ivoox.com/escuchar-escuela-selfcoaching-360_nq_321561_1.html" target="_blank"><img src="<?php bloginfo( 'template_url' ); ?>/images/iconos/ivoox.png" alt="ivoox" /></a>
                </p>
                
                
<!--
            <div class="shareSinglePost">
				<a href="#"><i class="fa fa-facebook"></i></a>
				<a href="#"><i class="fa fa-twitter"></i></a>
				<a href="#"><i class="fa fa-instagram"></i></a>
				<a href="#"><i class="fa fa-pinterest"></i></a>
			</div>
-->


            </div>
            

		</div>