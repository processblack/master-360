<?php get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	<section class="container">
		<div class="pageHeader" style="background-color: #555; background-image: url(<?php echo  get_the_post_thumbnail_url($post->ID, 'cabecera'); ?>); background-position: center bottom">
			<h1><?php the_title(); ?></h1>
		</div>
		
		
        <?php
        $postID = $post->ID;
        $categorias = get_categories(array('hide_empty' => 0));
        ?>
            
            <div class="pagePanel clear">
            	<ul class="productFilter  clear">
                	
                	
                	<li id="parent-<?php the_ID(); ?>" class="parent-page primer">
                	    <strong>CATEGORIAS:</strong>
                	</li>
                	
                <?php foreach ($categorias as $categoria){ ?>
            
                    <li id="parent-<?php the_ID(); ?>" class="parent-page">
                        <a href="<?php echo get_category_link($categoria->term_id); ?>"><?php echo $categoria->name; ?></a>
                        
                    </li>
                <?php } ?>
            	</ul>
            </div>	
       
		
        	
		<div id="blog">
    		<div class="wrapper">
    			<div class="singlePostWrap">    
            		<?php the_content();?>
            		
            
                <div class="social-share">
                    <p><strong>Compartir este post</strong></p>
                    <a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank" class="ir fb"><img style="opacity: .6;" src="<?php bloginfo( 'template_url' ); ?>/images/iconos/facebook.png" alt="fb" width="24" height="24"></a> 
                    <a href="https://twitter.com/share?url=<?php the_permalink(); ?>&text=<?php echo the_title(); ?> @selfcoaching360" target="_blank" class="ir tw"><img style="opacity: .6;" src="<?php bloginfo( 'template_url' ); ?>/images/iconos/twitter.png" alt="tw" width="24" height="24"></a> 
                    <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&title=<?php the_title(); ?>" target="_blank" class="ir gp"> <img style="opacity: .6;" src="<?php bloginfo( 'template_url' ); ?>/images/iconos/linkedin.png" alt="goo" width="24" height="24"></a>     

                </div>
            		
            		
            		
				<div class="singlePostTags" style="text-transform: uppercase !important; line-height: 1.4em;">
                    
                    <strong>Escrito por: </strong>
                    <a class="url fn n" href="<?php echo get_the_author_link(); ?>" title="Legazpi6(r)en bidalketa guztiak ikusi"><?php echo get_the_author(); ?></a><br />
                    
                    <strong>Fecha</strong>: 
                    <a href="<?php echo get_post_permalink(); ?>" title="8:23 am" rel="bookmark"><?php the_time('Y, F j')?></a>
                    | 
                    					
                    
					<strong>Categorías:</strong>
					|
					<a href="<?php echo get_post_permalink().'#comments'?>">Comentarios</a>
				</div>
						
						
				<div id="comentarios">
				<?php comments_template( '', true ); ?>
				</div>
            		
            		
            		
                </div>
                <div id="sidebar">
                    <?php if ( is_active_sidebar( 'sidebar-blog' ) ) : ?>  
                         <div id="widget-area" class="widget-area">  
                            <?php dynamic_sidebar( 'sidebar-blog' ); ?>  
                         </div>  
                    <?php endif; ?>
                </div>
            </div>
        </div>
	    <div style="clear: both">	
        <?php ETG_contenido(get_the_ID()); ?>
        </div>
        <?php #require_once('includes/descarga-dossier.php'); ?>
        <?php #require_once('includes/formulario.php'); ?>
		
	</section>


<?php endwhile; ?>
<?php get_footer(); ?>





