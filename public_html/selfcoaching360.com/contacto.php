<?php require_once('includes/config.php'); ?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<title>Self Coaching 360º - Desarrollo personal y liderazgo</title>
		<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
		<!--[if lte IE 8]>
		<script src="<?php echo ETG_BASE_URL; ?>/http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<!--[if lt IE 8]>
			<script src="<?php echo ETG_BASE_URL; ?>/http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
		<![endif]-->
		<link rel="shortcut icon" href="<?php echo ETG_BASE_URL; ?>/favicon.ico" type="image/x-icon" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/bxslider.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/font-awesome.min.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/selectric.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/style.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/adaptive.css" media="screen" />
		
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/jquery.selectric.min.js"></script>
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/jquery.bxslider.min.js"></script>
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/script.js"></script>
	</head>

<body class="single-post">
    
    <?php 
        $current = 'master-360';
        require_once('includes/cabecera.php');
    ?>

	<section class="container">
		<div class="pageHeader" style="background-image: url(images/contacto.jpg); background-position: center bottom;">
		</div>
		<div class="wrapper">
			<div class="singlePostWrap">    
					<h3>Contacto</h3>
					<p>
    					ESCUELA DE DESARROLLO PERSONAL Y LIDERAZGO<br />
                        Iker Fernández<br />
                        Director
                    </p>
                    <p>
                        Ainhoa Sagarna / Eliana Fernández<br />
                        Equipo de Coordinación
                    </p>
					<p><i class="contactPhone"></i> +34 607119660</p>
					<p><i class="contactEmail"></i> master@selfcoaching360.com</p>
            </div>
        </div>
	    <?php require_once('includes/descarga-dossier.php'); ?>
	
    <?php require_once('includes/formulario.php'); ?>
	</section>

    <?php require_once('includes/pie.php'); ?>   
    
</body>


</html>

