		<div id="formulario" class="classesBox-formulario">
			<h2 class="center uppercase">¿Te interesa el Máster?</h2>
			<p class="center">Solicita tu entrevista personal. En breve nos pondremos en contacto contigo.</p>
			<form id="form1" name="form1" method="post" action="" class="center-block">    			
				<p class="izquierda">
					<label for="nombre">Nombre</label>
					<input type="text" name="nombre" id="nombre" />
				</p>
				<p class="derecha">
					<label for="telefono">Teléfono</label>
					<input type="text" name="telefono" id="telefono" />
				</p>
				<p class="izquierda">
					<label for="localidad">Localidad</label>
					<input type="text" name="localidad" id="localidad" />
				</p>
				<p class="derecha">
                    <label for="email">E-mail</label>
					<input type="text" name="email" id="email" />
				</p>
				
				<p style="clear: both">
                <label>
                    <input type="checkbox" id="checkbox_aceptar" value="si"> Acepto la <a href="#" target="_blank" style="color: white;">política de privacidad</a> del sitio.
                </label>
				</p>
				<div id="resultadoMensaje"></div>
				
				
				<p>
                    <input type="hidden" id="formulario" name="formulario" value="info" />
                    <input type="hidden" id="url" name="url" value="" />
                    <input type="submit" name="enviar" id="enviar_formulario" value="Enviar" class="enviar" />
				</p>
			</form>
		</div>



