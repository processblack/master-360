<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-89756104-2', 'auto');
  ga('send', 'pageview');

</script>	
	
	
	<header id="header">
		<div class="headerWrap clear">
			<a href="<?php echo ETG_BASE_URL; ?>/#" class="logo">
				<!-- You can also use image as a logo the example below -->
				<img class="logo-white" src="<?php echo ETG_BASE_URL; ?>/images/coaching-logo-white.png" srcset="<?php echo ETG_BASE_URL; ?>/images/coaching-logo-white@2x.png 2x" alt="Self Coaching 360" />
				<img class="logo-black" src="<?php echo ETG_BASE_URL; ?>/images/coaching-logo.png" srcset="<?php echo ETG_BASE_URL; ?>/images/coaching-logo@2x.png 2x" alt="Self Coaching 360" />
				</svg>
			</a>
			<nav class="mainMenu">
				<ul class="clear">
					<li<?php if ($current == 'home') { echo ' class="current-menu-item"'; }; ?>>
    					<a href="<?php echo ETG_BASE_URL; ?>/index.php">home</a>
                    </li>
					<li<?php if ($current == 'master-360') { echo ' class="current-menu-item"'; }; ?>>
						<a href="<?php echo ETG_BASE_URL; ?>/master-360/">Master 360º</a>
						<ul>
            				<li><a href="<?php echo ETG_BASE_URL; ?>/master-360/origen-selfcoaching.php">El origen</a></li>
            				<li><a href="<?php echo ETG_BASE_URL; ?>/master-360/master-de-masteres.php">Un máster de másteres</a></li>
            				<li><a href="<?php echo ETG_BASE_URL; ?>/master-360/te-interesa-si.php">Te interesa si...</a></li>
            				<li><a href="<?php echo ETG_BASE_URL; ?>/master-360/que-lo-hace-especial.php">CARACTERÍSTICAS</a></li>
            				<li><a href="<?php echo ETG_BASE_URL; ?>/master-360/experiencia-a-la-carta.php">Experiencia a la carta</a></li>
            				<li><a href="<?php echo ETG_BASE_URL; ?>/master-360/genuinos.php">Qué nos hace genuinos</a></li>
            				<li><a href="<?php echo ETG_BASE_URL; ?>/master-360/metodologia-experiencial.php">Metodología experiencial</a></li>
            				<li><a href="<?php echo ETG_BASE_URL; ?>/master-360/el-master-en-numeros.php">El máster en números</a></li>
            				<li><a href="<?php echo ETG_BASE_URL; ?>/master-360/poderosos-programas.php">5 Poderosos programas</a></li>
            				<li><a href="<?php echo ETG_BASE_URL; ?>/master-360/informacion.php">Dónde, cómo, cuánto, por qué</a></li>
						</ul>
					</li>
					<li<?php if ($current == 'equipo') { echo ' class="current-menu-item"'; }; ?>>
						<a href="<?php echo ETG_BASE_URL; ?>/equipo.php">Equipo</a>
					</li>
					<li<?php if ($current == 'eventos') { echo ' class="current-menu-item"'; }; ?>>
					    <a href="<?php echo ETG_BASE_URL; ?>/eventos.php">Agenda</a>
                    </li>
<!--
					<li<?php if ($current == 'eventos') { echo ' class="current-menu-item"'; }; ?>>
					    <a href="<?php echo ETG_BASE_URL; ?>/mas-informacion.php">Más información</a>
                    </li>
-->
					<li<?php if ($current == 'contacto') { echo ' class="current-menu-item"'; }; ?>>
					    <a href="<?php echo ETG_BASE_URL; ?>/contacto.php">Contacto</a>
                    </li>
				</ul>
			</nav>
			<span class="showMobileMenu">
				<span></span>
				<span></span>
				<span></span>
				<span></span>
			</span>
		</div>
	</header>