	<footer id="footer" class="clear">
		<div class="footerSocial clear">
			<a href="https://www.facebook.com/Selfcoaching360/"><i class="fa fa-facebook"></i></a>
			<a href="https://twitter.com/selfcoaching360 "><i class="fa fa-twitter"></i></a>
			<a href="https://www.instagram.com/selfcoaching360/ "><i class="fa fa-instagram"></i></a>
		</div>
		<ul class="footerMenu clear">
			<li<?php if ($current == 'home') { echo ' class="current-menu-item"'; }; ?>>
				<a href="<?php echo ETG_BASE_URL; ?>/index.php">home</a>
            </li>
			<li<?php if ($current == 'master-360') { echo ' class="current-menu-item"'; }; ?>>
				<a href="<?php echo ETG_BASE_URL; ?>/master-360/">Master 360º</a>
			</li>
			<li<?php if ($current == 'equipo') { echo ' class="current-menu-item"'; }; ?>>
				<a href="<?php echo ETG_BASE_URL; ?>/equipo.php">Equipo</a>
			</li>
			<li<?php if ($current == 'eventos') { echo ' class="current-menu-item"'; }; ?>>
			    <a href="<?php echo ETG_BASE_URL; ?>/eventos.php">Agenda</a>
            </li>
			<li<?php if ($current == 'contacto') { echo ' class="current-menu-item"'; }; ?>>
			    <a href="<?php echo ETG_BASE_URL; ?>/contacto.php">Contacto</a>
            </li>
		</ul>
<!--
		<div class="footerSubscribe">
			<form>
				<input class="" type="text" name="" size="20" value="" placeholder="Your email">
				<input class="btnSubscribe" type="submit" value="Subscribe">
			</form>
		</div>
-->
		<div class="copyright">
			<p>2017. Máster Self Coaching 360º</p>
		</div>
	</footer>
	<div class="mobileMenu">
		<ul>
			<li<?php if ($current == 'home') { echo ' class="current-menu-item"'; }; ?>>
				<a href="<?php echo ETG_BASE_URL; ?>/index.php">home</a>
            </li>
			<li<?php if ($current == 'master-360') { echo ' class="current-menu-item"'; }; ?>>
				<a href="<?php echo ETG_BASE_URL; ?>/master-360/">Master 360º</a>
				<ul>
    				<li><a href="<?php echo ETG_BASE_URL; ?>/master-360/origen-selfcoaching.php">El origen</a></li>
    				<li><a href="<?php echo ETG_BASE_URL; ?>/master-360/master-de-masteres.php">Un máster de másteres</a></li>
    				<li><a href="<?php echo ETG_BASE_URL; ?>/master-360/te-interesa-si.php">Te interesa si...</a></li>
    				<li><a href="<?php echo ETG_BASE_URL; ?>/master-360/que-lo-hace-especial.php">CARACTERÍSTICAS</a></li>
    				<li><a href="<?php echo ETG_BASE_URL; ?>/master-360/experiencia-a-la-carta.php">Experiencia a la carta</a></li>
    				<li><a href="<?php echo ETG_BASE_URL; ?>/master-360/genuinos.php">Qué nos hace genuinos</a></li>
    				<li><a href="<?php echo ETG_BASE_URL; ?>/master-360/metodologia-experiencial.php">Metodología experiencial</a></li>
    				<li><a href="<?php echo ETG_BASE_URL; ?>/master-360/el-master-en-numeros.php">El máster en números</a></li>
    				<li><a href="<?php echo ETG_BASE_URL; ?>/master-360/poderosos-programas.php">5 Poderosos programas</a></li>
    				<li><a href="<?php echo ETG_BASE_URL; ?>/master-360/informacion.php">Dónde, cómo, cuánto, por qué</a></li>
				</ul>
			</li>
			<li<?php if ($current == 'equipo') { echo ' class="current-menu-item"'; }; ?>>
				<a href="<?php echo ETG_BASE_URL; ?>/equipo.php">Equipo</a>
			</li>
			<li<?php if ($current == 'eventos') { echo ' class="current-menu-item"'; }; ?>>
			    <a href="<?php echo ETG_BASE_URL; ?>/eventos.php">Agenda</a>
            </li>
			<li<?php if ($current == 'contacto') { echo ' class="current-menu-item"'; }; ?>>
			    <a href="<?php echo ETG_BASE_URL; ?>/contacto.php">Contacto</a>
            </li>
		</ul>
	</div>
