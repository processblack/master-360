<?php require_once('includes/config.php'); ?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<title>Self Coaching 360º Desarrollo personal y liderazgo</title>
		<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
		<!--[if lte IE 8]>
		<script src="<?php echo ETG_BASE_URL; ?>/http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<!--[if lt IE 8]>
			<script src="<?php echo ETG_BASE_URL; ?>/http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
		<![endif]-->
		<link rel="shortcut icon" href="<?php echo ETG_BASE_URL; ?>/favicon.ico" type="image/x-icon" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/bxslider.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/font-awesome.min.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/selectric.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/style.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/adaptive.css" media="screen" />
		
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/jquery.selectric.min.js"></script>
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/jquery.bxslider.min.js"></script>
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/script.js"></script>
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/main.js"></script>
	</head>
<body class="single-post">
    
    <?php 
        $current = 'descarga-dossier';
        require_once('includes/cabecera.php');
    ?>

	<section class="container">
		<div class="pageHeader" style="background-image: url(<?php echo ETG_BASE_URL; ?>/images/home/slide-01.jpg);">
			<h1>Dossier Selfcoaching 360</h1>
		</div>
		
    	
		<div class="wrapper">
			<div class="singlePostWrap">    
    		</div>
    		
    		
		<div id="formulario-dossier">
			<p class="center">Rellena el siguiente formulario y te enviaremos un enlace a tu mail para que puedas descargar el dossier en formato pdf.</p>
			<form id="form-dossi" name="form1" method="post" action="" class="center-block">    			
				<p class="">
					<label for="nombre">Nombre</label>
					<input type="text" name="nombre" id="nombre" />
				</p>
				<p class="">
					<label for="telefono">Teléfono</label>
					<input type="text" name="telefono" id="telefono" />
				</p>
				<p class="">
					<label for="localidad">Localidad</label>
					<input type="text" name="localidad" id="localidad" />
				</p>
				<p class="">
                    <label for="mail">E-mail</label>
					<input type="text" name="mail" id="email" />
				</p>
				<p>
                <label>
                    <input type="checkbox" id="checkbox_aceptar" value="si"> Acepto la <a href="#" target="_blank" style="color: #7f7f7f">política de privacidad</a> del sitio.
                </label>
				</p>
				<div id="resultadoMensaje"></div>
				<p>
                    <input type="hidden" id="formulario" name="formulario" value="dossier" />
                    <input type="hidden" id="url" name="url" value="" />
                    <input type="submit" name="enviar" id="enviar_formulario" value="Enviar" class="enviar" />
				</p>
			</form>
		</div>
    		
		</div>
		
		
	</section>

    <?php require_once('includes/pie.php'); ?>
    
</body>
</html>