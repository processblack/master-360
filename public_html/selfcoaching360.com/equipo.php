<?php require_once('includes/config.php'); ?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<title>Self Coaching 360º Desarrollo personal y liderazgo</title>
		<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
		<!--[if lte IE 8]>
		<script src="<?php echo ETG_BASE_URL; ?>/http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<!--[if lt IE 8]>
			<script src="<?php echo ETG_BASE_URL; ?>/http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
		<![endif]-->
		<link rel="shortcut icon" href="<?php echo ETG_BASE_URL; ?>/favicon.ico" type="image/x-icon" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/bxslider.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/font-awesome.min.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/selectric.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/style.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/adaptive.css" media="screen" />
		
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/jquery.selectric.min.js"></script>
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/jquery.bxslider.min.js"></script>
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/script.js"></script>
	</head>
<body class="single-post">
    
    <?php 
        $current = 'equipo';
        require_once('includes/cabecera.php');
    ?>

	<section class="container">
		<div class="pageHeader" style="background-image: url(<?php echo ETG_BASE_URL; ?>/images/equipo.jpg);">
			<h1>UN EQUIPO DE AUTéNTICO LUJO</h1>
		</div>
		
		
    	
		<div class="wrapper">
			<div class="singlePostWrap">    
    			<p>Eres una persona muy importante para nosotros. Por eso mimamos mucho la calidad y experiencia del equipo de profesionales que te presentamos. Tienes que saber que cada persona que forma parte de este sensacional equipo cuenta con estas cinco cualidades:</p>
    			<ol>
                    <li>Personas cercanas y accesibles.</li>
                    <li>Inquietas y expertas en el tema del que hablan.</li>
                    <li>Experimentadas. Cuentan con una historia real que avala su trayectoria 	y discurso.</li>
                    <li>Destacan por ser buenas comunicadoras.</li>
                    <li>Ponen foco en que su formación sea experiencial y 100% práctica.</li>
                </ol>
    		</div>
		</div>
		
		<div class="ourTeam">
			<div class="teamItemWrap clear">
    			
        <?php
            $datos = array(
                array( 
                    'nombre' => 'Iker Fernández',
                    'titulo' => 'Director del Máster.',
                    'descrp' => 'Formador, conferenciante y emprendedor de la vida. Fundador de la Escuela Selfcoaching 360, Licenciado en Ciencias de la Información especializado en Marketing y Publicidad por la Universidad de Navarra. Coach profesional por el IESEC Human y creador del método Selfcoaching SDCS©. Y sobre todo, Hacedor de Aventuras.',
                    'foto_1' => 'iker-fernandez.jpg',
                    'foto_2' => 'iker-fernandez.png'
                ),
                array(
                    'nombre' => 'Ainhoa Sagarna',
                    'titulo' => 'Equipo de coordinación de la Escuela Selfcoaching 360.',
                    'descrp' => 'Equipo de coordinación de la Escuela Selfcoaching 360. Ingeniera industrial especializada en el ámbito de la organización y experiencia como project manager en empresas industriales. Máster en Dirección y Administración de empresas. Apasionada con todo lo relacionado con el desarrollo personal.  Le encantan las relaciones humanas y el trabajo en equipo. Buscadora de proyectos ilusionantes, blogger y co-creadora de “Buscando un cambio”.',
                    'foto_1' => 'ainhoa-sagarna.jpg',
                    'foto_2' => 'ainhoa-sagarna.png'
                    ),
                array(
                    'nombre' => 'Eliana Fernández',
                    'titulo' => 'Equipo de coordinación de la escuela Selfcoaching 360.',
                    'descrp' => 'Equipo de coordinación de la escuela Selfcoaching 360. Titulada en Secretariado Internacional. Apasionada de su familia y aprendiz de la vida. Blogger y co-creadora del blog “Buscando un cambio”.',
                    'foto_1' => 'eliana-fernandez.jpg',
                    'foto_2' => 'eliana-fernandez.jpg'
                    ),
                
                array(
                    'nombre' => 'Raimon Samsó',
                    'titulo' => 'Licenciado en Ciencias Económicas',
                    'descrp' => 'Licenciado en Ciencias Económicas, coach financiero para emprendedores y director del Instituto de Expertos. Autor del libro éxito de ventas “El código del Dinero”, ha escrito más de una docena de libros sobre desarrollo personal. Colaborador habitual en el programa de Catalunya Radio L´ofici de viure, de Gaspar Hernández, y en El País Semanal.',
                    'foto_1' => 'raimon-samso.jpg',
                    'foto_2' => 'raimon-samso.png'),
                
/*
                array(
                    'nombre' => 'Victor Küppers',
                    'titulo' => 'Formador y conferenciante. ',
                    'descrp' => 'Licenciado en Administración y Dirección de Empresas y Doctor en Humanidades. Imparte clases de Dirección Comercial en la Universidad Internacional de Cataluña y la Universidad de Barcelona.',
                    'foto_1' => 'default.jpg',
                    'foto_2' => 'default.jpg'
                    ),
*/
                array(
                    'nombre' => 'Hana Kanjaa',
                    'titulo' => 'Video-blogger y malabarista del cambio. ',
                    'descrp' => 'Video-blogger y malabarista del cambio. Se define más mentora que coach, conferenciante “accidental” y mamá de dos niñas. Licenciada en Publicidad y RRPP. Coach certificada ICF. Su vida, dice, ha sido una constante búsqueda en torno a la pregunta: ¿qué hago con mi vida?.',
                    'foto_1' => 'hana-kanjaa.jpg',
                    'foto_2' => 'hana-kanjaa.png'
                    ),
/*
                array(
                    'nombre' => 'Mónica Fusté',
                    'titulo' => 'Fundadora del Instituto de SuperAcción y de Coach Premium. ',
                    'descrp' => 'Emprendedora, autora de cuatro libros de desarrollo personal y conferenciante. Coach & mentor de alto impacto certi cada en Estados Unidos, economista y experta en reinvención profesional. Licenciada en Ciencias Económicas y Empresariales, Máster en Finanzas Internacionales.',
                    'foto_1' => 'monica-fuste.jpg',
                    'foto_2' => 'monica-fuste.png'
                    ),
*/
                array(
                    'nombre' => 'Luis Oiarzabal',
                    'titulo' => 'Fundador del movimiento TPA Tú puedes hacerlo y socio de la empresa Kreenti. ',
                    'descrp' => 'Fundador del movimiento TPA Tú puedes hacerlo y socio de la empresa Kreenti. Le apasionan las relaciones personales, hablar sobre retos y ayudar a otras personas a encaminarse en la dirección de sus valores. Coach profesional formado en inteligencia emocional, PNL y Neurocoaching.',
                    'foto_1' => 'luis-oiarzabal.jpg',
                    'foto_2' => 'luis-oiarzabal.png'
                    ),
                array(
                    'nombre' => 'Elena Palomo',
                    'titulo' => 'Médico de Familia, Psiquiatra y Psicoterapeuta Humanista. ',
                    'descrp' => 'Médico de Familia, Psiquiatra y Psicoterapeuta Humanista. Formada en Gestalt, EMDR, Psicoterapia basada en Mindfulness, y Psicoterapia Dinámica Breve. Instructora de Mindfulness en el Instituto de Psicología Integral Baraka, en Donostia, en el cual forma parte de la Junta Ejecutiva, y del Equipo de Formadores de Instructores de Mindfulness. Socia fundadora y miembro de la junta directiva de la asociación Meditación y Ciencia.',
                    'foto_1' => 'elena-palomo.jpg',
                    'foto_2' => 'elena-palomo.png'
                    ),
                array(
                    'nombre' => 'María Soto',
                    'titulo' => 'Directora de la Escuela Frank Cardelle, coach profesional e instructora del NCI (Neurocoaching Institute). ',
                    'descrp' => 'Directora de la Escuela Frank Cardelle, coach profesional e instructora del NCI (Neurocoaching Institute). Consultora en Gaventerprise Group Corporation Fort Lauderdale, EEUU. Entrenadora en Psicoterapia y Dinámica de Grupos. Autora de Propuestas Pedagógicas Constructivistas para la Secretaría de Educación en Colombia; Terapeuta y asesora en procesos de recuperación de adicciones.',
                    'foto_1' => 'maria-soto.jpg',
                    'foto_2' => 'maria-soto.png'
                    ),
                array(
                    'nombre' => 'Enhamed Enhamed',
                    'titulo' => 'Ciego desde los ocho años, aprendió a nadar a los nueve, convirtiéndose desde los trece en una prioridad. ',
                    'descrp' => 'Ciego desde los ocho años, aprendió a nadar a los nueve, convirtiéndose desde los trece en una prioridad. Al finalizar sus segundos juegos paralímpicos en Beijing 2008 donde logró cuatro medallas de oro, quiso compartir todo lo que había significado el deporte para él, así como las claves que le llevaron a lograr sus metas. Primero como conferenciante y luego como coach deportivo y ejecutivo.',
                    'foto_1' => 'enhamed.jpg',
                    'foto_2' => 'enhamed.png'
                    ),
                array(
                    'nombre' => 'Pilar Jericó',
                    'titulo' => 'Empresaria, escritora y conferenciante',
                    'descrp' => 'Empresaria, escritora y conferenciante, reconocida como una de las Top Mujeres Líderes en España en la categoría de pensadoras y expertas. Pionera internacional en el análisis del talento y el impacto del miedo en las organizaciones y en las personas.',
                    'foto_1' => 'pilar-jerico.jpg',
                    'foto_2' => 'pilar-jerico.png'
                    ),
                array(
                    'nombre' => 'Paula Sopeña',
                    'titulo' => 'Coach, formadora, bloguera y madre. ',
                    'descrp' => 'Coach, formadora, bloguera y madre. Lidera la Escuela Europea de Líderes en Asturias y escribe en su blog ‘DreamsCoachTrue’. Ponente en UNED Campus Noroeste dentro de los programas de Coaching, Inteligencia Emocional y PNL. Coach Life y Coach ejecutivo.',
                    'foto_1' => 'paula-sopena.jpg',
                    'foto_2' => 'paula-sopena.png'
                    ),
                array(
                    'nombre' => 'ángel de Lope',
                    'titulo' => 'Arquitecto. Director de DSHumano',
                    'descrp' => 'Arquitecto. Director de DSHumano (Organización para el Desarrollo Sistémico Humano). Coach experto, pionero y líder en Coaching Sistémico HS® y Configuraciones Organizacionales. Escritor, conferenciante y profesor en las universidades UCM, USP-CEU, UEM, UPV, USC, UCA y EOI. Instructor de Mindfulness y de Consciencia Corporal. Ha desarrollado su propia metodología denominada “Sistémica-HS”®.',
                    'foto_1' => 'angel-de-lope.jpg',
                    'foto_2' => 'angel-de-lope.png'
                    ),
                array(
                    'nombre' => 'Gustavo Bertolotto',
                    'titulo' => 'Economista',
                    'descrp' => 'Economista, coach experto y formador pionero en Programación Neurolinguística. (PNL). Introdujo la enseñanza de la PNL en España en 1989. Fundador de la AEPNL, socio del Instituto de Potencial Humano y creador de la PNL Transpersonal.',
                    'foto_1' => 'gustavo-bertolotto.jpg',
                    'foto_2' => 'gustavo-bertolotto.png'
                    ),
                array(
                    'nombre' => 'Pilar Feijoo',
                    'titulo' => 'Licenciada en Pedagogía.',
                    'descrp' => 'Licenciada en Pedagogía. Terapeuta de familia, individual y de pareja. Experta en Sandplay therapy, Análisis transacional, PNL, Eneagrama, Constelaciones familiares e Intervenciones Sistémicas en la Organización. Más de 35 años de experiencia en formación.',
                    'foto_1' => 'pilar-feijoo.jpg',
                    'foto_2' => 'pilar-feijoo.png'
                    ),
                array(
                    'nombre' => 'Silvia Álava',
                    'titulo' => 'Psicóloga',
                    'descrp' => 'Psicóloga, experta en Logopedia por el Instituto de la Comunicación y Lenguaje. Directora del área infantil del Centro de Psicología Álava Reyes. Profesora del Practicum de Psicología de la Universidad Autónoma de Madrid y del Máster en Psicología Infanto-Juvenil del ISPCS. Autora del libro “Queremos hijos felices” y coautora de la enciclopedia “La Psicología que nos ayuda a vivir”.',
                    'foto_1' => 'silvia-alava.jpg',
                    'foto_2' => 'silvia-alava.png'
                    ),
                array(
                    'nombre' => 'Javier Iriondo',
                    'titulo' => 'Emprendedor, escritor, conferenciante y formador.',
                    'descrp' => 'Emprendedor, escritor, conferenciante y formador. Deportista profesional en EEUU desde 1987 a 1990. Autor que ha impartido numerosas conferencias sobre liderazgo y transformación personal por países como Italia, Francia, Brasil, EEUU, Argentina, Puerto Rico, Portugal y España. Autor del bestseller “Donde tus sueños te lleven”.',
                    'foto_1' => 'javier-iriondo.jpg',
                    'foto_2' => 'javier-iriondo.png'
                    ),
                array(
                    'nombre' => 'Ander Urruticoechea',
                    'titulo' => 'Director Científico del centro Onkologikoa.',
                    'descrp' => 'Director Científico del centro Onkologikoa. Doctor en Medicina por la Universidad de Barcelona. Especialista en Oncología Médica. Autor de múltiples publicaciones en revistas internacionales sobre la biología molecular del cáncer de mama y la búsqueda de nuevas dianas terapéuticas en tumores resistentes al tratamiento convencional.',
                    'foto_1' => 'ander-urruticoechea.jpg',
                    'foto_2' => 'ander-urruticoechea.png'
                    ),
                array(
                    'nombre' => 'Karmelo Bizkarra',
                    'titulo' => 'Licenciado en Medicina y cirugía por la UPV.',
                    'descrp' => 'Licenciado en Medicina y cirugía por la UPV. Director médico del Centro de Salud Vital Zuhaizpe. Máster en Bioenergética y formación en teoría Reichiana. Su trabajo se centra en desarrollar un programa de integración de la salud física con la salud psicoemocional. Presidente de ASYMI, Asociación de Salud y Medicina Integrativa.',
                    'foto_1' => 'karmelo-bizkarra.jpg',
                    'foto_2' => 'karmelo-bizkarra.png'
                    ),
                array(
                    'nombre' => 'Rakel Ampudia',
                    'titulo' => 'Directora de la Escuela de Biodanza del País vasco.',
                    'descrp' => 'Directora de la Escuela de Biodanza del País vasco. Facilitadora Didacta de Biodanza titulada por la IBF. Trabajadora Social (UPV), Postgrado de Psicología Sistémica (UPV), Máster en Excelencia Gerencial (U. Deusto), Master en PNL, Analista Bioenergética, Coach ontológica Transformacional y Coach Sistémica.',
                    'foto_1' => 'rakel-ampudia.jpg',
                    'foto_2' => 'rakel-ampudia.png'
                    ),
/*
                array(
                    'nombre' => 'Aitor Alberdi Rodríguez',
                    'titulo' => 'Licenciado en Ciencias de la Actividad Física y el Deporte.',
                    'descrp' => 'Licenciado en Ciencias de la Actividad Física y el Deporte. Entrenador Nacional de Triatlón y Ciclismo. Miembro del cuerpo técnico de BAT Basque Team.',
                    'foto_1' => 'default.jpg',
                    'foto_2' => 'default.jpg'
                    ),
*/
                array(
                    'nombre' => 'Gabriela Retana',
                    'titulo' => 'Licenciada en Farmacia, Nutrición y Dietética.',
                    'descrp' => 'Licenciada en Farmacia, Nutrición y Dietética por la Universidad de Navarra. Nutricionista del primer equipo de la Real Sociedad de Fútbol en la primera división de la La Liga de fútbol profesional, labor que compagina con su consulta privada. Fue dietista colaboradora del Club Atlético Osasuna en la temporada 2014. Actualmente especializándose en Nutrición deportiva por el COI (Comité Olímpico Internacional), máxima acreditación para el asesoramiento deportivo de cualquier atleta profesional.',
                    'foto_1' => 'gabriela-retana.jpg',
                    'foto_2' => 'gabriela-retana.png'
                    ),
                array(
                    'nombre' => 'Iosu Lázcoz',
                    'titulo' => 'Creador del método Optitud.',
                    'descrp' => 'Creador del método Optitud. Conferenciante y autor especializado en motivación. Licenciado en Biología y Máster en Gestión Comercial y Marketing por la Universidad de Navarra. Dispone de la suficiencia investigadora por la UPNA. Es Practitioner en Positive Psychology Coaching por el IEPP. Socio fundador del proyecto “Felicidad Sostenible” y socio fundador de los “Congresos Vender Hoy”.',
                    'foto_1' => 'iosu-lakoz.jpg',
                    'foto_2' => 'iosu-lakoz.png'
                    ),
                array(
                    'nombre' => 'Imanol Ibarrondo',
                    'titulo' => 'Fundador y Presidente de INCOADE.',
                    'descrp' => 'Fundador y Presidente de INCOADE. Ex futbolista profesional, Licenciado en Ciencias Empresariales, Máster de gestión de entidades deportivas por la UPV y coach por la ICF. Coach de entrenadores para el COE los pasados JJOO de Brasil y actual coach en la selección de fútbol de México.',
                    'foto_1' => 'imanol-ibarrondo.jpg',
                    'foto_2' => 'imanol-ibarrondo.png'
                    ),
                array(
                    'nombre' => 'María Iturriaga',
                    'titulo' => 'Profesora de la Deusto Business School. ',
                    'descrp' => 'Profesora de la Deusto Business School. Licenciada en Ciencias Económicas y Empresariales, Máster en Recursos Humanos por la Universidad de Deusto. Coach por la ICF y Doctorada en Competitividad y Desarrollo Económico. Comprometida con ayudar a otros a ser mejores líderes cada día, en ámbitos como carrera profesional, liderazgo y competencias directivas.',
                    'foto_1' => 'maria-iturriaga.jpg',
                    'foto_2' => 'maria-iturriaga.png'
                    ),
                array(
                    'nombre' => 'José Manuel Torres',
                    'titulo' => 'Fundador de Planifica tu éxito.',
                    'descrp' => 'Fundador de Planifica tu éxito. Especializado en estrategia y planificación. Ingeniero por la Universidad de Penn State (EEUU) y Doctor en seguridad informática por Tecnun. Profesor de grado y postgrado, empresario, coach, consultor y conferenciante. Creador del método de las 7P para la empresa.',
                    'foto_1' => 'jose-manuel-torres.jpg',
                    'foto_2' => 'jose-manuel-torres.png'
                    ),
                array(
                    'nombre' => 'Claudia Chackelson',
                    'titulo' => 'Ingeniera y Doctora en Logísticas',
                    'descrp' => 'Ingeniera y Doctora en Logísticas por el Departamento de Organización Industrial de la Universidad de Navarra. Inversora, Trader y Directora de Planifica tus Finanzas. Trainer de Investment Mastery, cursos de Stock Market, Forex y Finanzas Personales.',
                    'foto_1' => 'claudia-chackelson.jpg',
                    'foto_2' => 'claudia-chackelson.png'
                    )
            );
            ?>    			
    		
    		

    			
				<?php 
    				$i = 1;
    				foreach ($datos as $ponente) { ?>
				<div data-userid="user_<?php echo $i; ?>" class="teamItem">
					<img src="images/ponentes/azules/<?php echo $ponente['foto_1']; ?>" alt="">
					<div class="overlay">
						<div class="teamItemNameWrap">
							<h3><?php echo $ponente['nombre']; ?></h3>
						</div>
						<p><?php echo $ponente['titulo']; ?></p>
					</div>
				</div>	

				
				<div id="user_<?php echo $i; ?>" class="teamItemDesc">
					<div class="teamItemDescWrap">
						<img src="images/ponentes/<?php echo $ponente['foto_1']; ?>" alt="" class="teamItemImg img-responsive center-block">
						<h3><?php echo $ponente['nombre']; ?></h3>
						<p class="teamItemDescText1"><?php echo $ponente['titulo']; ?></p>
						<p class="teamItemDescText"><?php echo $ponente['descrp']; ?></p>
<!--
						<div class="teamItemSocial">
							<a href="#"><i class="fa fa-facebook"></i></a>
							<a href="#"><i class="fa fa-twitter"></i></a>
							<a href="#"><i class="fa fa-pinterest"></i></a>
						</div>
-->
					</div>
					<span class="closeTeamDesc">
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" width="16px" height="16px" viewBox="1.5 -1 16 16" enable-background="new 1.5 -1 16 16" xml:space="preserve">
							<path fill="#C1F4E8" d="M11.185 7l6.315 6.314L15.814 15L9.5 8.685L3.186 15L1.5 13.314L7.815 7L1.5 0.686L3.186-1L9.5 5.3 L15.814-1L17.5 0.686L11.185 7z"/>
						</svg>
					</span>
				</div>				
				<?php 
    				$i++; 
                } ?>
				
				
            </div>
        </div>
		
    <?php require_once('includes/descarga-dossier.php'); ?>
		
    <?php require_once('includes/formulario.php'); ?>
		
	</section>

    <?php require_once('includes/pie.php'); ?>   
    
</body>
</html>