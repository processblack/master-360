<?php require_once('includes/config.php'); ?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<title>Self Coaching 360º Desarrollo personal y liderazgo</title>
		<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
		<!--[if lte IE 8]>
		<script src="<?php echo ETG_BASE_URL; ?>/http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<!--[if lt IE 8]>
			<script src="<?php echo ETG_BASE_URL; ?>/http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
		<![endif]-->
		<link rel="shortcut icon" href="<?php echo ETG_BASE_URL; ?>/favicon.ico" type="image/x-icon" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/bxslider.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/font-awesome.min.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/selectric.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/style.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ETG_BASE_URL; ?>/css/adaptive.css" media="screen" />
		
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/jquery.selectric.min.js"></script>
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/jquery.bxslider.min.js"></script>
		<script type="text/javascript" src="<?php echo ETG_BASE_URL; ?>/js/script.js"></script>
	</head>
<body class="single-post">
    
    <?php 
        $current = 'equipo';
        require_once('includes/cabecera.php');
    ?>

	<section class="container">
		<div class="pageHeader" style="background-image: url(<?php echo ETG_BASE_URL; ?>/images/girasoles/cabecera.jpg);">
			<h1 style="top: 180px;">
    			ENTRE  GIRASOLES Y VIÑEDOS
            </h1>
		</div>
		
		
    	
		<div class="wrapper">
			<div class="singlePostWrap">
    			<h4 style="font-family: 'Roboto Condensed', sans-serif;">UNA EXPERIENCIA INOLVIDABLE 
DE LA ESCUELA SELF COACHING 360º 
DEL PAÍS VASCO</h4>
    			<p>SelfCoaching 360º entre girasoles y viñedos es una experiencia única diseñada por la Escuela de desarrollo personal y liderazgo SelfCoaching 360º.</p>
    			<p>Durante 6 días asistirás al curso “DESARROLLAR MI ESCUCHA ACTIVA” (Maestría IAC®) que será impartida por el director de nuestra escuela, Iker Fernández, mientras disfrutas de un entorno inspirador y relajante, rodeado de fantásticos paisajes y exquisita gastronomía.</p>
    			<p>Un lugar mágico donde volver a reencontrarte con tu mejor yo.</p>
            </div>
    			
        </div>
    			
    			
    			
				
		<div class="pageHeader" style="background-image: url(<?php echo ETG_BASE_URL; ?>/images/girasoles/comida.jpg);">

		</div>
				
        <div class="wrapper">
    			
			<div class="singlePostWrap">
    			
				<h4 style="font-family: 'Roboto Condensed', sans-serif;">¿CUÁNDO Y DONDE?</h4>
				<p><strong>DEL 26 DE JUNIO AL 1 DE JULIO DE 2018.</strong></p>
				<p>La experiencia  SELF COACHING 360º ENTRE GIRASOLES Y VIÑEDOS se realizará en el Departamento de Gers, al sur de Francia. Considerado el corazón de la Gascuña, es una región histórica de extraordinaria belleza también conocida por su exquisita gastronomía.</p>
				<p>Hemos reservado en exclusiva para el grupo una finca privada de 4 hectáreas con piscina, espacios relax, puestas de sol inolvidables y una fauna muy divertida de la que podrás disfrutar durante este curso.</p>
    			
				<h4 style="font-family: 'Roboto Condensed', sans-serif;">TARIFAS EXPERIENCIA</h4>
    		


	
    			
    			<table class="table">
        		    <tr>
            		    <th></th>
            		    <th>SOY ALUMN@ Curso + Experiencias</th>
            		    <th>ACOMPAÑO A ALUMN@ Experiencias</th>
                    </tr>
                    <tr>
                        <td class="text-left">Hasta el 30 de Noviembre</td>
                        <td class="text-center">995€</td>
                        <td rowspan="3" class="text-center">495€**</td>
                    </tr>
                    <tr>
                        <td class="text-left">Hasta el 1 de Febrero 2018</td>
                        <td class="text-center">1.100€</td>
                    </tr>
                    <tr>
                        <td class="text-left">A partir del 1 de Febrero 2018</td>
                        <td class="text-center">1.375€</td>
                    </tr>
                </table>
                <p style="margin-bottom: 60px;">**PVP en habitación doble en régimen MP.</p>
                
                
                <div style="float: left;">
                    <p><strong>Organizan</strong></p>
                    <p>
                        <img src="<?php echo ETG_BASE_URL; ?>/images/girasoles/organizan.png" alt="Selfcoaching 360 - Turista Gastronómico" />
                    </p>
                </div>
                <div style="float: right;">
                    <p><strong>Colaboran</strong></p>
                    <p>
                        <img src="<?php echo ETG_BASE_URL; ?>/images/girasoles/colaboran.png" alt="Selfcoaching 360 - Turista Gastronómico" />
                    </p>
                </div>
                <div style="clear: both"></div>
                
                
                <p style="text-align: center;"><a href="https://www.eventbrite.es/e/entradas-desayunando-buenos-cereales-gestion-de-personas-equipos-40788593782" target="_blank" class="descarga">INSCRIBIRME</a></p>
                
    			
            </div>
        </div>
    			
    			
    			
				
		<div class="pageHeader" style="background-image: url(<?php echo ETG_BASE_URL; ?>/images/girasoles/personas.jpg);">
			<h1 class="">Este es el plan</h1>
		</div>
				
        <div class="wrapper">
			<div class="singlePostWrap">    
		
                <div class="numero">
                <h4>1.</h4>
                <p><strong>Aprender</strong> con uno de los <strong>mejores formadores</strong> de la Escuela SelfCoaching 360º la <strong>Maestría IAC® “DESARROLLAR MI ESCUCHA ACTIVA”</strong>.</p>
                </div>
                
                <div class="numero">
                <h4>2.</h4>
                <p><strong>Descubrir</strong> un destino inolvidable, conocido popularmente como la <strong>“Toscana” Francesa</strong>, de la mano de una turista gastronómica muy curiosa, que te mostrará sus pequeños tesoros. </p>
                </div>
                
                <div class="numero">
                <h4>3.</h4>
                <p>Alojarte en una preciosa <strong>finca rural</strong> llena de espacios y rincones donde perderte… o también  donde unirte a otros viajeros SelfCoaching 360º.</p>
                </div>
                
                <div class="numero">
                <h4>4.</h4>
                <p>Olvidarte del móvil, de las prisas, del estrés y <strong>disfrutar mientras aprendes</strong>. ¡Bienvenid@!</p>

                </div>
                <p>&nbsp;</p>
            </div>
        </div>
				
				

    			
    			
		<div class="pageHeader" style="background-image: url(<?php echo ETG_BASE_URL; ?>/images/girasoles/larresingle.jpg);">
			<h1 class="">¿Qué incluye?</h1>
		</div>
    			
		<div class="wrapper">
			<div class="singlePostWrap">    
				
				<p><strong>Formación intensiva de 20 horas</strong> en la Maestría IAC® <strong>“DESARROLLAR MI ESCUCHA ACTIVA”</strong> de la Escuela SelfCoaching 360º que incluye:</p>
				<p>— Desarrollo de habilidades para facilitar relaciones interpersonales.</p>
				<p>— Práctica de las 3S: saber escuchar, saber hablar, saber contrastar.</p>
				<p>— Claves para crear espacios de comunicación enriquecedores con personas y equipos.</p>
				<p><strong>6 días/5 noches</strong> en régimen de media pensión (desayuno + almuerzo) y disponibilidad de cocina para cenas.</p>
				<p><strong>Experiencias gastronómicas auténticas</strong> con visitas y degustaciones.</p>
				<p><strong>Asistencia 24h</strong> por parte del personal de la Escuela SelfCoaching 360º.</p>
				
				
				<p>El programa no incluye el desplazamiento.</p>
    			
            </div>
        </div>
    			
		<div class="wrapper">
			<div class="singlePostWrap">    
				<h4 style="font-family: 'Roboto Condensed', sans-serif;">RECOMENDADO PARA:</h4>
    			
    			<p>Personas con inquietud en el desarrollo personal y ganas de vivir una nueva experiencia de formación fuera del entorno habitual.</p>
    			<p>*Si te interesa, coméntanoslo lo antes posible ya que las plazas son muy limitadas.</p>
    			
                <p style="text-align: center;"><a href="https://www.eventbrite.es/e/entradas-desayunando-buenos-cereales-gestion-de-personas-equipos-40788593782" target="_blank" class="descarga">INSCRIBIRME</a></p>    			
    			
    			
            </div>
        </div>

	</section>

    <?php require_once('includes/pie.php'); ?>   
    
</body>
</html>